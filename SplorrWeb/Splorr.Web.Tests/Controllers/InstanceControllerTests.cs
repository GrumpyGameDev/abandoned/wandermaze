﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Splorr.ApplicationServices;
using Splorr.Web.Controllers;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Splorr.Web.Tests.Controllers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class InstanceControllerTests
    {
        [TestMethod]
        [TestProperty("Area", "MVC Controller")]
        [TestProperty("Service", "HomeController")]
        [TestProperty("Operation", "Index")]
        public void InstanceController_Index()
        {
            const int page = 10;
            const int pageSize = 20;
            const string instanceNameFilter = "name";
            const Domain.InstanceType typeFilter = Domain.InstanceType.Build;
            const bool publicFilter = true;
            const bool closedFilter = false;
            const bool lockedFilter = true;
            const Domain.InstanceUserLevel levelFilter = Domain.InstanceUserLevel.Builder;
            const string userId = "user";

            Mock<IInstanceService> instanceService = new Mock<IInstanceService>();


            InstanceController controller = new InstanceController(instanceService.Object, Utility.StandardMapper);


            var result = controller.Index(page, pageSize, instanceNameFilter, typeFilter, publicFilter, closedFilter, lockedFilter, levelFilter);

            Assert.IsNotNull(result);

            instanceService.Verify(x => x.GetUserInstances(page, pageSize, instanceNameFilter, typeFilter, publicFilter, closedFilter, lockedFilter, levelFilter, userId), Times.Once);
            instanceService.Verify(x => x.GetUserInstanceCount(instanceNameFilter, typeFilter, publicFilter, closedFilter, lockedFilter, levelFilter, userId), Times.Once);
        }
    }
}
