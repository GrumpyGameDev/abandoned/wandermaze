﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Splorr.Web;
using Splorr.Web.Controllers;
using System.Diagnostics.CodeAnalysis;

namespace Splorr.Web.Tests.Controllers
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class HomeControllerTest
    {
        [TestMethod]
        [TestProperty("Area", "MVC Controller")]
        [TestProperty("Service", "HomeController")]
        [TestProperty("Operation", "Index")]
        public void HomeController_Index()
        {
            // Arrange
            HomeController controller = new HomeController();

            // Act
            ViewResult result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }

    }
}
