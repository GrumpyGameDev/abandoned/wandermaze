﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr.Domain;
using Splorr.DomainServices;
using System.Diagnostics;

namespace Splorr.ApplicationServices
{
    public class InstanceService : ServiceBase, IInstanceService
    {
        private readonly IInstanceRepository _instanceRepository;
        private readonly IInstanceUserRepository _instanceUserRepository;
        private readonly IUserRepository _userRepository;
        private readonly IUserInstanceRepository _userInstanceRepository;
        public InstanceService(
            IInstanceRepository instanceRepository,
            IInstanceUserRepository instanceUserRepository,
            IUserRepository userRepository,
            IUserInstanceRepository userInstanceRepository,
            ILogger logger
            )
            :base(logger)
        {
            _instanceRepository = instanceRepository;
            _instanceUserRepository = instanceUserRepository;
            _userRepository = userRepository;
            _userInstanceRepository = userInstanceRepository;
        }

        public ApplicationServiceResponse Create(string instanceName, bool closed, string userId)
        {
            return DoStuff(() =>
            {
                Logger.Diagnostic("Checking that the user is in good standing.");
                if (!_userRepository.IsUser(userId))
                {
                    Logger.Error("User is not in good standing.");
                    return ApplicationServiceErrorResponse.Create(ErrorMessages.PermissionDenied);
                }

                Logger.Diagnostic("Checking that the instance name is unique.");
                if (!_instanceRepository.IsNameUnique(instanceName, null))
                {
                    Logger.Error("Instance name is not unique.");
                    return ApplicationServiceErrorResponse.Create(ErrorMessages.UniqueInstanceName);
                }

                Logger.Diagnostic("Creating the instance.");
                Instance instance = _instanceRepository.Create(
                    new Instance()
                    {
                        InstanceName = instanceName,
                        InstanceType = InstanceType.Build,
                        Closed = closed,
                        Public = false,
                        Locked = false
                    });

                if (instance == null)
                {
                    Logger.Critical("Instance could not be created.");
                    return ApplicationServiceErrorResponse.Create(ErrorMessages.CouldNotCreateInstance);
                }

                Logger.Diagnostic("Logging instance creation.");
                Logger.ChangeEvent(null, instance);

                Logger.Diagnostic("Making user an owner of the instance.");
                InstanceUser instanceUser = _instanceUserRepository.Create(
                    new InstanceUser()
                    {
                        InstanceId = instance.InstanceId,
                        UserId = userId,
                        Level = InstanceUserLevel.Owner
                    });

                if (instanceUser == null)
                {
                    Logger.Critical("Instance user could not be created");
                    return ApplicationServiceErrorResponse.Create(ErrorMessages.CouldNotCreateInstanceUser);
                }

                Logger.Diagnostic("Logging instance user creation.");
                Logger.ChangeEvent(null, instanceUser);

                Logger.Informational("Created new instance and instance owner successfully.");
                return ApplicationServiceResponse<Instance>.Create(ApplicationServiceResult.Success, instance);
            });
        }

        public ApplicationServiceResponse Clone(int originalInstanceId, InstanceType instanceType, string instanceName, string userId)
        {
            throw new NotImplementedException();
        }

        public ApplicationServiceResponse Delete(int instanceId, string userId)
        {
            throw new NotImplementedException();
        }

        public ApplicationServiceResponse FindById(int instanceId, string userId)
        {
            throw new NotImplementedException();
        }

        public ApplicationServiceResponse UpdateClosedStatus(int instanceId, bool isClosed, string userId)
        {
            throw new NotImplementedException();
        }

        public ApplicationServiceResponse UpdateLockedStatus(int instanceId, bool isLocked, string userId)
        {
            throw new NotImplementedException();
        }

        public ApplicationServiceResponse UpdateName(int instanceId, string instanceName, string userId)
        {
            throw new NotImplementedException();
        }

        public ApplicationServiceResponse UpdatePublicStatus(int instanceId, bool isPublic, string userId)
        {
            throw new NotImplementedException();
        }

        public ApplicationServiceResponse SetUserLevel(int instanceId, string targetUserId, InstanceUserLevel level, string userId)
        {
            throw new NotImplementedException();
        }

        public ApplicationServiceResponse GetUserInstances(int page, int pageSize, string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool? closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId)
        {
            return ApplicationServiceListResponse<Domain.UserInstance>.Create(
                ApplicationServiceResult.Success,
                _userInstanceRepository.Find(
                    CalculateSkip(page, pageSize),
                    pageSize,
                    instanceNameFilter,
                    typeFilter,
                    publicFilter,
                    closedFilter,
                    lockedFilter,
                    levelFilter,
                    userId,
                    x => x.InstanceName,
                    false));
        }

        public ApplicationServiceResponse GetUserInstanceCount(string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool? closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId)
        {
            return ApplicationServiceResponse<int>.Create(
                ApplicationServiceResult.Success, 
                _userInstanceRepository.Count(
                    instanceNameFilter, 
                    typeFilter, 
                    publicFilter, 
                    closedFilter, 
                    lockedFilter, 
                    levelFilter, 
                    userId));
        }
    }
}
