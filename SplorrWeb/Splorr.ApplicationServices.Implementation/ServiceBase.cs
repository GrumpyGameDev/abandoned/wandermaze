﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr.DomainServices;
using System.Diagnostics;

namespace Splorr.ApplicationServices
{
    public abstract class ServiceBase
    {
        private readonly ILogger _logger;
        public ServiceBase(ILogger logger)
        {
            _logger = logger;
        }
        protected ILogger Logger
        {
            get
            {
                return _logger;
            }
        }
        protected ApplicationServiceResponse DoStuff(Func<ApplicationServiceResponse> func)
        {
            string frameName = new StackFrame(1).GetMethod().Name;
            _logger.Debug(string.Format("Entering '{0}'", frameName));
            ApplicationServiceResponse response = func();
            _logger.Debug(string.Format("Leaving '{0}'", frameName));
            return response;
        }
        protected static int CalculateSkip(int page, int pageSize)
        {
            return page <= 1 ? 0 : (page - 1) * pageSize;
        }
    }
}
