﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.Domain
{
    public class Instance
    {
        public int InstanceId { get; set; }
        public string InstanceName { get; set; }
        public bool Public { get; set; }
        public bool Closed { get; set; }
        public InstanceType InstanceType { get; set; }
        public bool Locked { get; set; }
    }
}
