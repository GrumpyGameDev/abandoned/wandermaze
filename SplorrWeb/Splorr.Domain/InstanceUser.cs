﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.Domain
{
    public class InstanceUser
    {
        public int InstanceId { get; set; }
        public string UserId { get; set; }
        public InstanceUserLevel Level { get; set; }
    }
}
