﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.Domain
{
    public class UserInstance
    {
        public int InstanceId { get; set; }
        public string InstanceName { get; set; }
        public InstanceType Type { get; set; }
        public bool Public { get; set; }
        public bool Closed { get; set; }
        public bool Locked { get; set; }
        public InstanceUserLevel Level { get; set; }
        public string UserId { get; set; }
    }
}
