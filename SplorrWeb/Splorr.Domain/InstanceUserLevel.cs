﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.Domain
{
    public enum InstanceUserLevel
    {
        None = 0,
        Tester = 1,
        Builder = 2,
        Owner = 3
    }
}
