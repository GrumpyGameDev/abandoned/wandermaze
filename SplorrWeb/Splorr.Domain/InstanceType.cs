﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.Domain
{
    public enum InstanceType
    {
        None = 0,
        Build = 1,
        Play = 2
    }
}
