﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using AutoMapper;

namespace Splorr.Web
{
    public static class DomainToModelMapping
    {
        internal static void CreateMaps(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Domain.UserInstance, Models.UserInstanceModel>()
                .ForMember(d=>d.InstanceType,o=>o.MapFrom(s=>s.Type));
        }
    }
}