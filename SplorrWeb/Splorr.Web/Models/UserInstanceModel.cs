﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Splorr.Web.Models
{
    public class UserInstanceModel
    {
        public int InstanceId { get; set; }
        public string InstanceName { get; set; }
        public bool Public { get; set; }
        public bool Closed { get; set; }
        public Domain.InstanceType InstanceType { get; set; }
        public bool Locked { get; set; }
    }
}