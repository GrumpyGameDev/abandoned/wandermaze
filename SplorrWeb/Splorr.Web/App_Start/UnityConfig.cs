using System;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;
using Splorr.ApplicationServices;
using Splorr.DomainServices;
using AutoMapper;
using System.Diagnostics.CodeAnalysis;

namespace Splorr.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    [ExcludeFromCodeCoverage]
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            var config = new MapperConfiguration(cfg => {
                DataToDomainMapping.CreateMaps(cfg);
                DomainToDataMapping.CreateMaps(cfg);
                DomainToModelMapping.CreateMaps(cfg);
                ModelToDomainMapping.CreateMaps(cfg);
            });
            container.RegisterInstance(config.CreateMapper());

            container.RegisterType<IInstanceRepository, InstanceRepository>();
            container.RegisterType<IInstanceService, InstanceService>();
            container.RegisterType<IInstanceUserRepository, InstanceUserRepository>();

            container.RegisterType<ILogger, Logger>();

            container.RegisterType<IUserInstanceRepository, UserInstanceRepository>();
            container.RegisterType<IUnitOfWorkFactory, EFUnitOfWorkFactory>();
            container.RegisterType<IUserRepository, UserRepository>();
        }
    }
}
