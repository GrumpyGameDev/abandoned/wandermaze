﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Splorr.Web
{
    public static class Utility
    {
        public static IMapper StandardMapper
        {
            get
            {
                var config = new MapperConfiguration(cfg => {
                    DomainToModelMapping.CreateMaps(cfg);
                    ModelToDomainMapping.CreateMaps(cfg);
                });
                return config.CreateMapper();
            }
        }
    }
}