﻿using Splorr.ApplicationServices;
using Splorr.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using AutoMapper;

namespace Splorr.Web.Controllers
{
    [Authorize(Roles ="User, Admin")]
    public class InstanceController : Controller
    {
        private readonly IInstanceService _service;
        private readonly IMapper _mapper;
        public InstanceController(IInstanceService service, IMapper mapper)
        {
            _service = service;
            _mapper = mapper;
        }
        // GET: Instance
        public ActionResult Index(int page=1, int pageSize=20, string instanceNameFilter="", InstanceType? typeFilter=null, bool? publicFilter = null, bool? closedFilter = null, bool? lockedFilter = null, InstanceUserLevel? levelFilter = null)
        {
            ViewBag.Page = page;
            ViewBag.PageSize = pageSize;
            ViewBag.Count = _service.GetUserInstanceCount(instanceNameFilter, typeFilter, publicFilter, closedFilter, lockedFilter, levelFilter, User.Identity.GetUserId()).ToPayload<int>();
            ViewBag.Pages = (ViewBag.Count + pageSize - 1) / pageSize;

            var list = _mapper.Map<IEnumerable<Models.UserInstanceModel>>(_service.GetUserInstances(page, pageSize, instanceNameFilter, typeFilter, publicFilter, closedFilter, lockedFilter, levelFilter, User.Identity.GetUserId()).ToListPayload<Domain.UserInstance>());

            return View(list);
        }
    }
}