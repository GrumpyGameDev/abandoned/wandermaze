﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Splorr.Web.Startup))]
namespace Splorr.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
