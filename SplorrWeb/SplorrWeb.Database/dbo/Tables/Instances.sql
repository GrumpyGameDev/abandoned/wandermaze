﻿CREATE TABLE [dbo].[Instances]
(
	[InstanceId] INT IDENTITY(1,1) NOT NULL,
	[InstanceName] NVARCHAR(100) NOT NULL, 
    [Public] BIT NOT NULL, 
    [Closed] BIT NOT NULL, 
    [InstanceTypeId] INT NOT NULL, 
    [Locked] BIT NOT NULL, 
    CONSTRAINT PK_Instances PRIMARY KEY(InstanceId)
)
