﻿CREATE TABLE [dbo].[InstanceUsers]
(
	[InstanceId] INT NOT NULL, 
    [UserId] NVARCHAR(128) NOT NULL, 
    [InstanceUserLevelId] INT NOT NULL,
	CONSTRAINT PK_InstanceUsers PRIMARY KEY (InstanceId, UserId),
	CONSTRAINT FK_InstanceUsers_Instances FOREIGN KEY (InstanceId) REFERENCES Instances(InstanceId),
	CONSTRAINT FK_InstanceUsers_UserId FOREIGN KEY (UserId) REFERENCES AspNetUsers(Id),
	CONSTRAINT FK_InstanceUsers_InstanceUserLevels FOREIGN KEY (InstanceUserLevelId) REFERENCES InstanceUserLevels(InstanceUserLevelId)
)
