﻿CREATE VIEW [dbo].[UserInstances]
	AS
SELECT
	i.InstanceId,
	i.InstanceName,
	i.InstanceTypeId [Type],
	i.[Public],
	i.Closed,
	i.Locked,
	iu.InstanceUserLevelId [Level],
	iu.UserId
FROM
	InstanceUsers iu
	JOIN Instances i ON iu.InstanceId=i.InstanceId
