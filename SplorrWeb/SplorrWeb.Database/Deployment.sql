﻿/*
Post-Deployment Script Template							
--------------------------------------------------------------------------------------
 This file contains SQL statements that will be appended to the build script.		
 Use SQLCMD syntax to include a file in the post-deployment script.			
 Example:      :r .\myfile.sql								
 Use SQLCMD syntax to reference a variable in the post-deployment script.		
 Example:      :setvar TableName MyTable							
               SELECT * FROM [$(TableName)]					
--------------------------------------------------------------------------------------
*/
IF (0=(SELECT COUNT(1) FROM InstanceUserLevels WHERE InstanceUserLevelId=1))
BEGIN
	INSERT INTO InstanceUserLevels(InstanceUserLevelId) VALUES (1)
END
IF (0=(SELECT COUNT(1) FROM InstanceUserLevels WHERE InstanceUserLevelId=2))
BEGIN
	INSERT INTO InstanceUserLevels(InstanceUserLevelId) VALUES (2)
END
IF (0=(SELECT COUNT(1) FROM InstanceUserLevels WHERE InstanceUserLevelId=3))
BEGIN
	INSERT INTO InstanceUserLevels(InstanceUserLevelId) VALUES (3)
END

IF (0=(SELECT COUNT(1) FROM InstanceTypes WHERE InstanceTypeId=1))
BEGIN
	INSERT INTO InstanceTypes(InstanceTypeId) VALUES (1)
END
IF (0=(SELECT COUNT(1) FROM InstanceTypes WHERE InstanceTypeId=2))
BEGIN
	INSERT INTO InstanceTypes(InstanceTypeId) VALUES (2)
END
