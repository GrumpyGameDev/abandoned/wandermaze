﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System.Linq.Expressions;

namespace Splorr.DomainServices.Implementation.Tests
{
    [TestClass]
    public class InstanceRepositoryTests
    {
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "InstanceRepository")]
        [TestProperty("Operation", "Create")]
        public void InstanceRepository_Create()
        {
            Domain.Instance instance = new Domain.Instance()
            {
                Closed = false,
                InstanceId = 0,
                InstanceName = "name",
                InstanceType = Domain.InstanceType.Build,
                Locked = false,
                Public = false
            };

            MockHolder<IUnitOfWork> uowHolder = new MockHolder<IUnitOfWork>(uow => { });

            MockUnitOfWorkFactory factory = new MockUnitOfWorkFactory(uowHolder.HoldAndSetup);
            factory.AddMockFunction<Domain.Instance>();

            IInstanceRepository repository = new InstanceRepository(factory.UnitOfWorkFactory, Utility.StandardMapper);

            Domain.Instance createdInstance = repository.Create(instance);

            Assert.IsNotNull(createdInstance);

            uowHolder.Verify(uow => 
            {
                uow.Verify(x => x.MarkNew(It.IsAny<Instance>()), Times.Once);
                uow.Verify(x => x.Commit(), Times.Once);
            });
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "InstanceRepository")]
        [TestProperty("Operation", "Create With Null")]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InstanceRepository_Create_WithNull()
        {
            Domain.Instance instance = null;

            MockHolder<IUnitOfWork> uowHolder = new MockHolder<IUnitOfWork>(uow => { });

            MockUnitOfWorkFactory factory = new MockUnitOfWorkFactory(uowHolder.HoldAndSetup);
            factory.AddMockFunction<Domain.Instance>();

            IInstanceRepository repository = new InstanceRepository(factory.UnitOfWorkFactory, Utility.StandardMapper);

            repository.Create(instance);//throws exception
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "InstanceRepository")]
        [TestProperty("Operation", "Is Name Unique For Creation")]
        public void InstanceRepository_IsNameUnique_ForCreation()
        {
            const string instanceName = "name";
            int? instanceId = null;

            MockHolder<IUnitOfWork> uowHolder = new MockHolder<IUnitOfWork>(uow => { });

            MockUnitOfWorkFactory factory = new MockUnitOfWorkFactory(uowHolder.HoldAndSetup);
            factory.AddMockFunction<bool>();

            IInstanceRepository repository = new InstanceRepository(factory.UnitOfWorkFactory, Utility.StandardMapper);

            var result = repository.IsNameUnique(instanceName, instanceId);

            Assert.IsFalse(result);

            uowHolder.Verify(uow => {
                uow.Verify(x => x.Exists(It.IsAny<Expression<Func<Instance, bool>>>()), Times.Once);
            });
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "InstanceRepository")]
        [TestProperty("Operation", "Is Name Unique For Update")]
        public void InstanceRepository_IsNameUnique_ForUpdate()
        {
            const string instanceName = "name";
            int? instanceId = 1;

            MockHolder<IUnitOfWork> uowHolder = new MockHolder<IUnitOfWork>(uow => { });

            MockUnitOfWorkFactory factory = new MockUnitOfWorkFactory(uowHolder.HoldAndSetup);
            factory.AddMockFunction<bool>();

            IInstanceRepository repository = new InstanceRepository(factory.UnitOfWorkFactory, Utility.StandardMapper);

            var result = repository.IsNameUnique(instanceName, instanceId);

            Assert.IsFalse(result);

            uowHolder.Verify(uow => {
                uow.Verify(x => x.Exists(It.IsAny<Expression<Func<Instance, bool>>>()), Times.Once);
            });
        }
    }
}
