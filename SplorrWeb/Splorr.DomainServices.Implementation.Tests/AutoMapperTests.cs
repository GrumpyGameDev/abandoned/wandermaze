﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using Splorr.DomainServices;
using System.Diagnostics.CodeAnalysis;

namespace Splorr.DomainServices.Implementation.Tests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class AutoMapperTests
    {
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "AutoMapper")]
        [TestProperty("Operation", "Data To Domain")]
        public void AutoMapper_DataToDomainMapping()
        {
            var config = new MapperConfiguration(cfg => {
                DataToDomainMapping.CreateMaps(cfg);
            });
            config.AssertConfigurationIsValid();
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "AutoMapper")]
        [TestProperty("Operation", "Domain To Data")]
        public void AutoMapper_DomainToDataMappingTest()
        {
            var config = new MapperConfiguration(cfg => {
                DomainToDataMapping.CreateMaps(cfg);
            });
            config.AssertConfigurationIsValid();
        }
    }
}
