﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using AutoMapper;
using System.Linq.Expressions;
using Moq;
using System.Collections.Generic;

namespace Splorr.DomainServices.Implementation.Tests
{
    [TestClass]
    public class UserInstanceRepositoryTests
    {
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "UserInstanceRepository")]
        [TestProperty("Operation", "Find With Nulls")]
        public void UserInstanceRepository_Find_FiltersAllNull()
        {
            const int skip = 0;
            const int take = 1;
            const string instanceNameFilter = null;
            Domain.InstanceType? typeFilter = null;
            bool? publicFilter = null;
            bool? closedFilter = null;
            bool? lockedFilter = null;
            const string userId = "user";
            Domain.InstanceUserLevel? levelFilter = null;
            Expression<Func<Domain.UserInstance, string>> order = x => x.InstanceName;
            const bool descending = true;

            MockHolder<IUnitOfWork> uowHolder = new MockHolder<IUnitOfWork>(uow=> { });

            var factory = new MockUnitOfWorkFactory(uowHolder.HoldAndSetup);
            factory.AddMockFunction<IEnumerable<UserInstance>>();

            IUserInstanceRepository repository = new UserInstanceRepository(factory.UnitOfWorkFactory, Utility.StandardMapper);

            var result = repository.Find(skip, take, instanceNameFilter, typeFilter, publicFilter, closedFilter, lockedFilter, levelFilter, userId, order, descending);

            uowHolder.Verify(uow => 
            {
                uow.Verify(x => x.Find(skip,take,
                    It.IsAny<Expression<Func<DomainServices.UserInstance, bool>>>(),
                    It.IsAny<Expression<Func<DomainServices.UserInstance, string>>>(),
                    descending), Times.Once);
            });
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "UserInstanceRepository")]
        [TestProperty("Operation", "Find")]
        public void UserInstanceRepository_Find()
        {
            const int skip = 0;
            const int take = 1;
            const string instanceNameFilter = "name";
            const Domain.InstanceType typeFilter = Domain.InstanceType.Build;
            const bool publicFilter = true;
            const bool closedFilter = true;
            const bool lockedFilter = false;
            const string userId = "user";
            const Domain.InstanceUserLevel levelFilter = Domain.InstanceUserLevel.Builder;
            Expression<Func<Domain.UserInstance, string>> order = x => x.InstanceName;
            const bool descending = true;

            MockHolder<IUnitOfWork> uowHolder = new MockHolder<IUnitOfWork>(uow => { });

            var factory = new MockUnitOfWorkFactory(uowHolder.HoldAndSetup);
            factory.AddMockFunction<IEnumerable<UserInstance>>();

            IUserInstanceRepository repository = new UserInstanceRepository(factory.UnitOfWorkFactory, Utility.StandardMapper);

            var result = repository.Find(skip, take, instanceNameFilter, typeFilter, publicFilter, closedFilter, lockedFilter, levelFilter, userId, order, descending);

            uowHolder.Verify(uow =>
            {
                uow.Verify(x => x.Find(skip, take,
                    It.IsAny<Expression<Func<DomainServices.UserInstance, bool>>>(),
                    It.IsAny<Expression<Func<DomainServices.UserInstance, string>>>(),
                    descending), Times.Once);
            });
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "UserInstanceRepository")]
        [TestProperty("Operation", "Count")]
        public void UserInstanceRepository_Count()
        {
            const string instanceNameFilter = "name";
            const Domain.InstanceType typeFilter = Domain.InstanceType.Build;
            const bool publicFilter = true;
            const bool closedFilter = true;
            const bool lockedFilter = false;
            const string userId = "user";
            const Domain.InstanceUserLevel levelFilter = Domain.InstanceUserLevel.Builder;

            MockHolder<IUnitOfWork> uowHolder = new MockHolder<IUnitOfWork>(uow => { });

            var factory = new MockUnitOfWorkFactory(uowHolder.HoldAndSetup);
            factory.AddMockFunction<int>();

            IUserInstanceRepository repository = new UserInstanceRepository(factory.UnitOfWorkFactory, Utility.StandardMapper);

            var result = repository.Count(instanceNameFilter, typeFilter, publicFilter, closedFilter, lockedFilter, levelFilter, userId);

            uowHolder.Verify(uow =>
            {
                uow.Verify(x => x.Count(It.IsAny<Expression<Func<DomainServices.UserInstance, bool>>>()),
                    Times.Once);
            });
        }
    }
}
