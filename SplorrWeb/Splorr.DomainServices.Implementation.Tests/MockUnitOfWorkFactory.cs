﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Moq;
using System.Diagnostics.CodeAnalysis;

namespace Splorr.DomainServices.Implementation.Tests
{
    [ExcludeFromCodeCoverage]
    public class MockUnitOfWorkFactory
    {
        private Mock<IUnitOfWorkFactory> _unitOfWorkFactory;
        private Mock<IUnitOfWork> _unitOfWork;
        public Mock<IUnitOfWorkFactory> Mock
        {
            get
            {
                return _unitOfWorkFactory;
            }
        }
        public IUnitOfWorkFactory UnitOfWorkFactory
        {
            get
            {
                return Mock.Object;
            }
        }
        public Mock<IUnitOfWork> MockUnitOfWork
        {
            get
            {
                return _unitOfWork;
            }
        }
        public MockUnitOfWorkFactory(Action<Mock<IUnitOfWork>> unitOfWorkInitializer)
        {
            _unitOfWork = new Mock<IUnitOfWork>();
            unitOfWorkInitializer(_unitOfWork);
            _unitOfWorkFactory = new Mock<IUnitOfWorkFactory>();
            _unitOfWorkFactory.Setup(x => x.WithUnitOfWork(It.IsAny<Action<IUnitOfWork>>())).Callback<Action<IUnitOfWork>>(a=> 
            {
                a(_unitOfWork.Object);
            });
        }
        public void AddMockFunction<TResult>()
        {
            _unitOfWorkFactory.Setup(x => x.WithUnitOfWork(It.IsAny<Func<IUnitOfWork,TResult>>())).Returns<Func<IUnitOfWork,TResult>>(f =>
            {
                return f(_unitOfWork.Object);
            });
        }
    }
}
