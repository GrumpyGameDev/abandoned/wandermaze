﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices.Implementation.Tests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class UserRepositoryTests
    {
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "UserRepository")]
        [TestProperty("Operation", "IsUser")]
        public void UserRepository_IsUser()
        {
            const string userId = "user";

            MockUnitOfWorkFactory unitOfWorkFactory = new MockUnitOfWorkFactory(uow => { });
            unitOfWorkFactory.AddMockFunction<bool>();

            IUserRepository repository = new UserRepository(unitOfWorkFactory.UnitOfWorkFactory, Utility.StandardMapper);

            bool result = repository.IsUser(userId);

            Assert.IsFalse(result);

            unitOfWorkFactory.MockUnitOfWork.Verify(x => x.Exists(It.IsAny<Expression<Func<AspNetUserRole, bool>>>()), Times.Once);
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "UserRepository")]
        [TestProperty("Operation", "IsUser")]
        public void UserRepository_IsAdmin()
        {
            const string userId = "user";

            MockUnitOfWorkFactory unitOfWorkFactory = new MockUnitOfWorkFactory(uow => { });
            unitOfWorkFactory.AddMockFunction<bool>();

            IUserRepository repository = new UserRepository(unitOfWorkFactory.UnitOfWorkFactory, Utility.StandardMapper);

            bool result = repository.IsAdmin(userId);

            Assert.IsFalse(result);

            unitOfWorkFactory.MockUnitOfWork.Verify(x => x.Exists(It.IsAny<Expression<Func<AspNetUserRole, bool>>>()), Times.Once);
        }
    }
}
