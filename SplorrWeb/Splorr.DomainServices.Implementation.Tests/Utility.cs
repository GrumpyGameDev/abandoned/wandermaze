﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices.Implementation.Tests
{
    public static class Utility
    {
        public static IMapper StandardMapper
        {
            get
            {
                var config = new MapperConfiguration(cfg => {
                    DataToDomainMapping.CreateMaps(cfg);
                    DomainToDataMapping.CreateMaps(cfg);
                });
                return config.CreateMapper();
            }
        }
    }
}
