﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Diagnostics.CodeAnalysis;

namespace Splorr.DomainServices.Implementation.Tests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class LoggerTests
    {
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "Logger")]
        [TestProperty("Operation", "No Routes")]
        public void Logger_NoRoutes()
        {
            const string message = "message";
            ILogger logger = new Logger();

            logger.Critical(message);
            logger.Error(message);
            logger.Warning(message);
            logger.Informational(message);
            logger.Debug(message);
            logger.Diagnostic(message);
            logger.ChangeEvent(message, message);//string is not a good example here, but the function isn't called anyway

            //yes, nothing happens.
            //this test is here to ensure no exceptions occur when leaving handlers unspecified for the logger
        }


        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "Logger")]
        [TestProperty("Operation", "Critical")]
        public void Logger_Critical()
        {
            const string message = "message";
            int counter = 0;
            ILogger logger = new Logger();
            (logger as Logger).OnCritical = s=>counter++;
            (logger as Logger).OnError = s => Assert.Fail();
            (logger as Logger).OnWarning = s => Assert.Fail();
            (logger as Logger).OnInformational = s => Assert.Fail();
            (logger as Logger).OnDebug = s => Assert.Fail();
            (logger as Logger).OnDiagnostic = s => Assert.Fail();
            (logger as Logger).OnChangeEvent = (a, b) => Assert.Fail();

            logger.Critical(message);
            Assert.AreEqual(1, counter);
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "Logger")]
        [TestProperty("Operation", "Error")]
        public void Logger_Error()
        {
            const string message = "message";
            int counter = 0;
            ILogger logger = new Logger();
            (logger as Logger).OnCritical = s => Assert.Fail();
            (logger as Logger).OnError = s => counter++;
            (logger as Logger).OnWarning = s => Assert.Fail();
            (logger as Logger).OnInformational = s => Assert.Fail();
            (logger as Logger).OnDebug = s => Assert.Fail();
            (logger as Logger).OnDiagnostic = s => Assert.Fail();
            (logger as Logger).OnChangeEvent = (a, b) => Assert.Fail();

            logger.Error(message);
            Assert.AreEqual(1, counter);
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "Logger")]
        [TestProperty("Operation", "Warning")]
        public void Logger_Warning()
        {
            const string message = "message";
            int counter = 0;
            ILogger logger = new Logger();
            (logger as Logger).OnCritical = s => Assert.Fail();
            (logger as Logger).OnError = s => Assert.Fail();
            (logger as Logger).OnWarning = s => counter++;
            (logger as Logger).OnInformational = s => Assert.Fail();
            (logger as Logger).OnDebug = s => Assert.Fail();
            (logger as Logger).OnDiagnostic = s => Assert.Fail();
            (logger as Logger).OnChangeEvent = (a, b) => Assert.Fail();

            logger.Warning(message);
            Assert.AreEqual(1, counter);
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "Logger")]
        [TestProperty("Operation", "Informational")]
        public void Logger_Informational()
        {
            const string message = "message";
            int counter = 0;
            ILogger logger = new Logger();
            (logger as Logger).OnCritical = s => Assert.Fail();
            (logger as Logger).OnError = s => Assert.Fail();
            (logger as Logger).OnWarning = s => Assert.Fail();
            (logger as Logger).OnInformational = s => counter++;
            (logger as Logger).OnDebug = s => Assert.Fail();
            (logger as Logger).OnDiagnostic = s => Assert.Fail();
            (logger as Logger).OnChangeEvent = (a, b) => Assert.Fail();

            logger.Informational(message);
            Assert.AreEqual(1, counter);
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "Logger")]
        [TestProperty("Operation", "Debug")]
        public void Logger_Debug()
        {
            const string message = "message";
            int counter = 0;
            ILogger logger = new Logger();
            (logger as Logger).OnCritical = s => Assert.Fail();
            (logger as Logger).OnError = s => Assert.Fail();
            (logger as Logger).OnWarning = s => Assert.Fail();
            (logger as Logger).OnInformational = s => Assert.Fail();
            (logger as Logger).OnDebug = s => counter++; 
            (logger as Logger).OnDiagnostic = s => Assert.Fail();
            (logger as Logger).OnChangeEvent = (a, b) => Assert.Fail();

            logger.Debug(message);
            Assert.AreEqual(1, counter);
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "Logger")]
        [TestProperty("Operation", "Diagnostic")]
        public void Logger_Diagnostic()
        {
            const string message = "message";
            int counter = 0;
            ILogger logger = new Logger();
            (logger as Logger).OnCritical = s => Assert.Fail();
            (logger as Logger).OnError = s => Assert.Fail();
            (logger as Logger).OnWarning = s => Assert.Fail();
            (logger as Logger).OnInformational = s => Assert.Fail();
            (logger as Logger).OnDebug = s => Assert.Fail();
            (logger as Logger).OnDiagnostic = s => counter++;
            (logger as Logger).OnChangeEvent = (a,b) => Assert.Fail();

            logger.Diagnostic(message);
            Assert.AreEqual(1, counter);
        }
        [TestMethod]
        [TestProperty("Area", "Domain Services")]
        [TestProperty("Service", "Logger")]
        [TestProperty("Operation", "Change Event")]
        public void Logger_ChangeEvent()
        {
            const string message = "message";
            int counter = 0;
            ILogger logger = new Logger();
            (logger as Logger).OnCritical = s => Assert.Fail();
            (logger as Logger).OnError = s => Assert.Fail();
            (logger as Logger).OnWarning = s => Assert.Fail();
            (logger as Logger).OnInformational = s => Assert.Fail();
            (logger as Logger).OnDebug = s => Assert.Fail();
            (logger as Logger).OnDiagnostic = s => Assert.Fail();
            (logger as Logger).OnChangeEvent = (a, b) => counter++;

            logger.ChangeEvent(message,message);
            Assert.AreEqual(1, counter);
        }
    }
}
