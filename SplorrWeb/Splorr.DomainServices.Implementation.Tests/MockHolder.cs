﻿using Moq;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices.Implementation.Tests
{
    [ExcludeFromCodeCoverage]
    public class MockHolder<THeld>
        where THeld : class
    {
        private Mock<THeld> _held;
        private Action<Mock<THeld>> _setup;

        public MockHolder(Action<Mock<THeld>> setup)
        {
            _setup = setup;
        }

        public void HoldAndSetup(Mock<THeld> held)
        {
            _held = held;
            _setup(_held);
        }

        public void Verify(Action<Mock<THeld>> verify)
        {
            verify(_held);
        }
    }
}
