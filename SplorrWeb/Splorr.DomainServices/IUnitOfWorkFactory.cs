﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public interface IUnitOfWorkFactory
    {
        TResult WithUnitOfWork<TResult>(Func<IUnitOfWork, TResult> func);
        void WithUnitOfWork(Action<IUnitOfWork> action);
    }
}
