﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr.Domain;

namespace Splorr.DomainServices
{
    public interface IInstanceUserRepository
    {
        InstanceUser Create(InstanceUser instanceUser);
    }
}
