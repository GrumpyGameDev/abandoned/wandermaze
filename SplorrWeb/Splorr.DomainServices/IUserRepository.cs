﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public interface IUserRepository
    {
        bool IsUser(string userId);
        bool IsAdmin(string userId);
    }
}
