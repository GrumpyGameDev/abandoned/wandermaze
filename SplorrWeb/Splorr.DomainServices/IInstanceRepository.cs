﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr.Domain;

namespace Splorr.DomainServices
{
    public interface IInstanceRepository
    {
        bool IsNameUnique(string instanceName, int? instanceId);
        Instance Create(Instance instance);
    }
}
