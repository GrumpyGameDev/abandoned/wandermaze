﻿using Splorr.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public interface IUserInstanceRepository
    {
        IEnumerable<UserInstance> Find<TKey>(int skip, int take, string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool? closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId, Expression<Func<UserInstance,TKey>> order, bool descending);
        int Count(string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool? closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId);
    }
}
