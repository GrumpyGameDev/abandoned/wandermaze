﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public interface ILogger
    {
        void Critical(string message);
        void Error(string message);
        void Warning(string message);
        void Informational(string message);
        void Debug(string message);
        void Diagnostic(string message);
        void ChangeEvent<TEntity>(TEntity originalState, TEntity modifiedState);
    }
}
