﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public interface IUnitOfWork
    {
        bool Exists<TDataEntity>(Expression<Func<TDataEntity, bool>> filter) where TDataEntity:class;
        void MarkNew<TDataEntity>(TDataEntity entity) where TDataEntity : class;
        void MarkDirty<TDataEntity>(TDataEntity entity) where TDataEntity : class;
        void MarkDeleted<TDataEntity>(TDataEntity entity) where TDataEntity : class;
        void Commit();
        IEnumerable<TDataEntity> Find<TDataEntity, TKey>(int skip, int take, Expression<Func<TDataEntity, bool>> filter, Expression<Func<TDataEntity, TKey>> order, bool descending) where TDataEntity: class;
        int Count<TDataEntity>(Expression<Func<TDataEntity, bool>> filter) where TDataEntity : class;
    }
}
