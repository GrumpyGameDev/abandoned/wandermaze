﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Splorr.DomainServices;
using System.Collections.Generic;
using System.Linq;
using System.Diagnostics.CodeAnalysis;

namespace Splorr.ApplicationServices.Implementation.Tests
{
    [TestClass]
    [ExcludeFromCodeCoverage]
    public class InstanceServiceTests
    {
        private class Mocks
        {
            public Mock<IInstanceRepository> InstanceRepository { get; set; }
            public Mock<IInstanceUserRepository> InstanceUserRepository { get; set; }
            public Mock<IUserRepository> UserRepository { get; set; }
            public Mock<IUserInstanceRepository> UserInstanceRepository { get; set; }
        }

        #region Utilities

        private void WithService(Action<Mocks> mocksAction, Action<IInstanceService, MockLogger, Mocks> action)
        {
            Mocks mocks = new Mocks()
            {
                InstanceRepository = new Mock<IInstanceRepository>(),
                InstanceUserRepository = new Mock<IInstanceUserRepository>(),
                UserRepository = new Mock<IUserRepository>(),
                UserInstanceRepository = new Mock<IUserInstanceRepository>()
            };

            mocksAction(mocks);

            MockLogger logger = new MockLogger();

            IInstanceService service = new InstanceService(
                mocks.InstanceRepository.Object,
                mocks.InstanceUserRepository.Object,
                mocks.UserRepository.Object,
                mocks.UserInstanceRepository.Object,
                logger.Logger);

            action(service, logger, mocks);
        }

        #endregion

        #region Create
        [TestMethod]
        [TestProperty("Area", "Application Services")]
        [TestProperty("Service", "InstanceService")]
        [TestProperty("Operation", "Create")]
        public void InstanceService_Create_UserNotInGoodStanding()
        {
            const string instanceName = "instance";
            const bool closed = false;
            const string userId = "user";

            WithService(mocks => { },
                (service, logger, mocks) =>
                {
                    ApplicationServiceResponse response = service.Create(instanceName, closed, userId);

                    Assert.IsNotNull(response);
                    Assert.AreEqual(ApplicationServiceResult.Failure, response.Result);

                    IEnumerable<string> messages = response.ToListPayload<string>();

                    Assert.IsNotNull(messages);
                    Assert.AreEqual(messages.Count(), 1);
                    Assert.IsTrue(messages.Any(x => x == ErrorMessages.PermissionDenied));

                    Assert.AreEqual(0, logger.CriticalLog.Count(), "Critical Count");
                    Assert.AreEqual(1, logger.ErrorLog.Count(), "Error Count");
                    Assert.AreEqual(0, logger.WarningLog.Count(), "Warning Count");
                    Assert.AreEqual(0, logger.InformationalLog.Count(), "Informational Count");
                    Assert.AreEqual(2, logger.DebugLog.Count(), "Debug Count");
                    Assert.AreEqual(1, logger.DiagnosticLog.Count(), "Diagnostic Count");

                    logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.Instance>()), Times.Never);
                    logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.InstanceUser>()), Times.Never);

                });
        }
        [TestMethod]
        [TestProperty("Area", "Application Services")]
        [TestProperty("Service", "InstanceService")]
        [TestProperty("Operation", "Create")]
        public void InstanceService_Create_InstanceNameNotUnique()
        {
            const string instanceName = "instance";
            const bool closed = false;
            const string userId = "user";

            WithService(mocks =>
            {
                mocks.UserRepository.Setup(x => x.IsUser(userId)).Returns(true);
            },
                (service, logger, mocks) =>
                {
                    ApplicationServiceResponse response = service.Create(instanceName, closed, userId);

                    Assert.IsNotNull(response);
                    Assert.AreEqual(ApplicationServiceResult.Failure, response.Result);

                    IEnumerable<string> messages = response.ToListPayload<string>();

                    Assert.IsNotNull(messages);
                    Assert.AreEqual(messages.Count(), 1);
                    Assert.IsTrue(messages.Any(x => x == ErrorMessages.UniqueInstanceName));

                    Assert.AreEqual(0, logger.CriticalLog.Count(), "Critical Count");
                    Assert.AreEqual(1, logger.ErrorLog.Count(), "Error Count");
                    Assert.AreEqual(0, logger.WarningLog.Count(), "Warning Count");
                    Assert.AreEqual(0, logger.InformationalLog.Count(), "Informational Count");
                    Assert.AreEqual(2, logger.DebugLog.Count(), "Debug Count");
                    Assert.AreEqual(2, logger.DiagnosticLog.Count(), "Diagnostic Count");

                    logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.Instance>()), Times.Never);
                    logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.InstanceUser>()), Times.Never);
                });
        }
        [TestMethod]
        [TestProperty("Area", "Application Services")]
        [TestProperty("Service", "InstanceService")]
        [TestProperty("Operation", "Create")]
        public void InstanceService_Create_CouldNotCreateInstance()
        {
            const string instanceName = "instance";
            const bool closed = false;
            const string userId = "user";

            WithService(mocks =>
            {
                mocks.InstanceRepository.Setup(x => x.IsNameUnique(instanceName, null)).Returns(true);

                mocks.UserRepository.Setup(x => x.IsUser(userId)).Returns(true);
            }, (service, logger, mocks) =>
            {
                ApplicationServiceResponse response = service.Create(instanceName, closed, userId);

                Assert.IsNotNull(response);
                Assert.AreEqual(ApplicationServiceResult.Failure, response.Result);

                IEnumerable<string> messages = response.ToListPayload<string>();

                Assert.IsNotNull(messages);
                Assert.AreEqual(messages.Count(), 1);
                Assert.IsTrue(messages.Any(x => x == ErrorMessages.CouldNotCreateInstance));

                Assert.AreEqual(1, logger.CriticalLog.Count(), "Critical Count");
                Assert.AreEqual(0, logger.ErrorLog.Count(), "Error Count");
                Assert.AreEqual(0, logger.WarningLog.Count(), "Warning Count");
                Assert.AreEqual(0, logger.InformationalLog.Count(), "Informational Count");
                Assert.AreEqual(2, logger.DebugLog.Count(), "Debug Count");
                Assert.AreEqual(3, logger.DiagnosticLog.Count(), "Diagnostic Count");

                logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.Instance>()), Times.Never);
                logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.InstanceUser>()), Times.Never);

            });
        }
        [TestMethod]
        [TestProperty("Area", "Application Services")]
        [TestProperty("Service", "InstanceService")]
        [TestProperty("Operation", "Create")]
        public void InstanceService_Create_CouldNotCreateInstanceUser()
        {
            const int instanceId = 1;
            const string instanceName = "instance";
            const bool closed = false;
            const string userId = "user";

            WithService(mocks =>
            {
                mocks.InstanceRepository.Setup(x => x.IsNameUnique(instanceName, null)).Returns(true);
                mocks.InstanceRepository.Setup(x => x.Create(It.IsAny<Domain.Instance>())).Returns<Domain.Instance>(i => { i.InstanceId = instanceId; return i; });

                mocks.UserRepository.Setup(x => x.IsUser(userId)).Returns(true);
            },
                (service, logger, mocks) =>
                {
                    ApplicationServiceResponse response = service.Create(instanceName, closed, userId);

                    Assert.IsNotNull(response);
                    Assert.AreEqual(ApplicationServiceResult.Failure, response.Result);

                    IEnumerable<string> messages = response.ToListPayload<string>();

                    Assert.IsNotNull(messages);
                    Assert.AreEqual(messages.Count(), 1);
                    Assert.IsTrue(messages.Any(x => x == ErrorMessages.CouldNotCreateInstanceUser));

                    Assert.AreEqual(1, logger.CriticalLog.Count(), "Critical Count");
                    Assert.AreEqual(0, logger.ErrorLog.Count(), "Error Count");
                    Assert.AreEqual(0, logger.WarningLog.Count(), "Warning Count");
                    Assert.AreEqual(0, logger.InformationalLog.Count(), "Informational Count");
                    Assert.AreEqual(2, logger.DebugLog.Count(), "Debug Count");
                    Assert.AreEqual(5, logger.DiagnosticLog.Count(), "Diagnostic Count");

                    logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.Instance>()), Times.Once);
                    logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.InstanceUser>()), Times.Never);
                });
        }
        [TestMethod]
        [TestProperty("Area", "Application Services")]
        [TestProperty("Service", "InstanceService")]
        [TestProperty("Operation", "Create")]
        public void InstanceService_Create_Success()
        {
            const int instanceId = 1;
            const string instanceName = "instance";
            const bool closed = false;
            const string userId = "user";

            WithService(mocks =>
            {
                mocks.InstanceRepository.Setup(x => x.IsNameUnique(instanceName, null)).Returns(true);
                mocks.InstanceRepository.Setup(x => x.Create(It.IsAny<Domain.Instance>())).Returns<Domain.Instance>(i => { i.InstanceId = instanceId; return i; });
                mocks.InstanceUserRepository.Setup(x => x.Create(It.IsAny<Domain.InstanceUser>())).Returns<Domain.InstanceUser>(i => i);
                mocks.UserRepository.Setup(x => x.IsUser(userId)).Returns(true);
            },
                (service, logger, mocks) =>
                {
                    ApplicationServiceResponse response = service.Create(instanceName, closed, userId);

                    Assert.IsNotNull(response);
                    Assert.AreEqual(ApplicationServiceResult.Success, response.Result);

                    Domain.Instance instance = response.ToPayload<Domain.Instance>();

                    Assert.IsNotNull(instance);
                    Assert.AreEqual(closed, instance.Closed, "Closed");
                    Assert.AreEqual(instanceId, instance.InstanceId, "InstanceId");
                    Assert.AreEqual(instanceName, instance.InstanceName, "InstanceName");
                    Assert.AreEqual(Domain.InstanceType.Build, instance.InstanceType, "InstanceType");
                    Assert.AreEqual(false, instance.Locked, "Locked");
                    Assert.AreEqual(false, instance.Public, "Public");

                    Assert.AreEqual(0, logger.CriticalLog.Count(), "Critical Count");
                    Assert.AreEqual(0, logger.ErrorLog.Count(), "Error Count");
                    Assert.AreEqual(0, logger.WarningLog.Count(), "Warning Count");
                    Assert.AreEqual(1, logger.InformationalLog.Count(), "Informational Count");
                    Assert.AreEqual(2, logger.DebugLog.Count(), "Debug Count");
                    Assert.AreEqual(6, logger.DiagnosticLog.Count(), "Diagnostic Count");

                    logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.Instance>()), Times.Once);
                    logger.Mock.Verify(x => x.ChangeEvent(null, It.IsAny<Domain.InstanceUser>()), Times.Once);
                });
        }
        #endregion
    }
}
