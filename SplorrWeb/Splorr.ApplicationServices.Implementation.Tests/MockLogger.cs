﻿using Moq;
using Splorr.DomainServices;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.ApplicationServices.Implementation.Tests
{
    [ExcludeFromCodeCoverage]
    public class MockLogger
    {
        private List<string> _criticalLog = new List<string>();
        private List<string> _errorLog = new List<string>();
        private List<string> _warningLog = new List<string>();
        private List<string> _informationalLog = new List<string>();
        private List<string> _debugLog = new List<string>();
        private List<string> _diagnosticLog = new List<string>();
        private Mock<ILogger> _logger = new Mock<ILogger>();
        public MockLogger()
        {
            _logger.Setup(x => x.Critical(It.IsAny<string>())).Callback<string>(m => _criticalLog.Add(m));
            _logger.Setup(x => x.Error(It.IsAny<string>())).Callback<string>(m => _errorLog.Add(m));
            _logger.Setup(x => x.Warning(It.IsAny<string>())).Callback<string>(m => _warningLog.Add(m));
            _logger.Setup(x => x.Informational(It.IsAny<string>())).Callback<string>(m => _informationalLog.Add(m));
            _logger.Setup(x => x.Debug(It.IsAny<string>())).Callback<string>(m => _debugLog.Add(m));
            _logger.Setup(x => x.Diagnostic(It.IsAny<string>())).Callback<string>(m => _diagnosticLog.Add(m));
        }
        public Mock<ILogger> Mock
        {
            get
            {
                return _logger;
            }
        }
        public ILogger Logger
        {
            get
            {
                return _logger.Object;
            }
        }
        public List<string> CriticalLog
        {
            get
            {
                return _criticalLog;
            }
        }
        public List<string> ErrorLog
        {
            get
            {
                return _errorLog;
            }
        }
        public List<string> WarningLog
        {
            get
            {
                return _warningLog;
            }
        }
        public List<string> InformationalLog
        {
            get
            {
                return _informationalLog;
            }
        }
        public List<string> DebugLog
        {
            get
            {
                return _debugLog;
            }
        }
        public List<string> DiagnosticLog
        {
            get
            {
                return _diagnosticLog;
            }
        }
    }
}
