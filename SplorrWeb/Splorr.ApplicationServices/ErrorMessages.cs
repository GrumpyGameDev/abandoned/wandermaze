﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.ApplicationServices
{
    public static class ErrorMessages
    {
        public const string PermissionDenied = "ErrorMessages.PermissionDenied";
        public const string UniqueInstanceName = "ErrorMessages.UniqueInstanceName";
        public const string CouldNotCreateInstance = "ErrorMessages.CouldNotCreateInstance";
        public const string CouldNotCreateInstanceUser = "ErrorMessages.CouldNotCreateInstanceUser";
    }
}
