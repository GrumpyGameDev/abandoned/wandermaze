﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr.Domain;

namespace Splorr.ApplicationServices
{
    public class ApplicationServiceResponse
    {
        public ApplicationServiceResult Result { get; private set; }
        protected ApplicationServiceResponse(ApplicationServiceResult result)
        {
            Result = result;
        }
    }
    public class ApplicationServiceResponse<TPayload> : ApplicationServiceResponse
    {
        public TPayload Payload { get; private set; }
        protected ApplicationServiceResponse(ApplicationServiceResult result, TPayload payload)
            : base(result)
        {
            Payload = payload;
        }

        public static ApplicationServiceResponse<TPayload> Create(ApplicationServiceResult result, TPayload payload)
        {
            return new ApplicationServiceResponse<TPayload>(result, payload);
        }
    }
    public class ApplicationServiceListResponse<TPayload> : ApplicationServiceResponse<IEnumerable<TPayload>>
    {
        protected ApplicationServiceListResponse(ApplicationServiceResult result, IEnumerable<TPayload> payload)
            : base(result, payload)
        {

        }
    }
    public class ApplicationServiceErrorResponse : ApplicationServiceListResponse<string>
    {
        protected ApplicationServiceErrorResponse(params string[] messages)
            : base(ApplicationServiceResult.Failure, messages)
        {

        }
        public static ApplicationServiceErrorResponse Create(params string[] messages)
        {
            return new ApplicationServiceErrorResponse(messages);
        }
    }

    public static class ApplicationServiceResponseExtensionMethods
    {
        public static bool IsSuccess(this ApplicationServiceResponse response)
        {
            return response.Result == ApplicationServiceResult.Success;
        }
        public static T ToPayload<T>(this ApplicationServiceResponse response)
        {
            var payloadResponse = response as ApplicationServiceResponse<T>;
            if(payloadResponse!=null)
            {
                return payloadResponse.Payload;
            }
            else
            {
                return default(T);
            }
        }
        public static IEnumerable<T> ToListPayload<T>(this ApplicationServiceResponse response)
        {
            return response.ToPayload<IEnumerable<T>>();
        }
    }

}
