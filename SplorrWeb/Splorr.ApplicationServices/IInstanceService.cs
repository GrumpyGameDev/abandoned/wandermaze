﻿using Splorr.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.ApplicationServices
{
    public interface IInstanceService
    {
        ApplicationServiceResponse Create(string instanceName, bool closed, string userId);
        ApplicationServiceResponse Clone(int originalInstanceId, InstanceType instanceType, string instanceName, string userId);
        ApplicationServiceResponse FindById(int instanceId, string userId);
        ApplicationServiceResponse UpdateName(int instanceId, string instanceName, string userId);
        ApplicationServiceResponse UpdatePublicStatus(int instanceId, bool isPublic, string userId);
        ApplicationServiceResponse UpdateClosedStatus(int instanceId, bool isClosed, string userId);
        ApplicationServiceResponse Delete(int instanceId, string userId);
        ApplicationServiceResponse UpdateLockedStatus(int instanceId, bool isLocked, string userId);
        ApplicationServiceResponse SetUserLevel(int instanceId, string targetUserId, InstanceUserLevel level, string userId);
        ApplicationServiceResponse GetUserInstances(int page, int pageSize, string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool?closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId);
        ApplicationServiceResponse GetUserInstanceCount(string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool? closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId);
    }
}
