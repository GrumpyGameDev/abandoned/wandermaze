﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public class Logger : ILogger
    {
        public Action<string> OnCritical { get; set; }
        public Action<string> OnError { get; set; }
        public Action<string> OnWarning { get; set; }
        public Action<string> OnInformational { get; set; }
        public Action<string> OnDebug { get; set; }
        public Action<string> OnDiagnostic { get; set; }
        public Action<object,object> OnChangeEvent { get; set; }

        public void Critical(string message)
        {
            OnCritical?.Invoke(message);
        }

        public void Debug(string message)
        {
            OnDebug?.Invoke(message);
        }

        public void Diagnostic(string message)
        {
            OnDiagnostic?.Invoke(message);
        }

        public void Error(string message)
        {
            OnError?.Invoke(message);
        }

        public void Informational(string message)
        {
            OnInformational?.Invoke(message);
        }

        public void Warning(string message)
        {
            OnWarning?.Invoke(message);
        }

        public void ChangeEvent<TEntity>(TEntity originalState, TEntity modifiedState)
        {
            OnChangeEvent?.Invoke(originalState, modifiedState);
        }
    }
}
