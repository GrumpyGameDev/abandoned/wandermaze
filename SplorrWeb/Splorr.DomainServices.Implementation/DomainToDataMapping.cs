﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Splorr.DomainServices
{
    public static class DomainToDataMapping
    {
        public static void CreateMaps(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Domain.Instance, Instance>()
                .ForMember(d => d.InstanceTypeId, o => o.MapFrom(s => (int)s.InstanceType))
                .ForMember(d => d.InstanceUsers, o => o.Ignore());
        }
    }
}
