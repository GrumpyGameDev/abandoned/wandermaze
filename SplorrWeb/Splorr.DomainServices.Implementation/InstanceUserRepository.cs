﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr.Domain;
using AutoMapper;

namespace Splorr.DomainServices
{
    public class InstanceUserRepository : RepositoryBase, IInstanceUserRepository
    {
        public InstanceUserRepository(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
            : base(unitOfWorkFactory, mapper)
        {

        }

        public Domain.InstanceUser Create(Domain.InstanceUser instanceUser)
        {
            return UnitOfWorkFactory.WithUnitOfWork(uow =>
            {
                return uow.StandardCreate<Domain.InstanceUser, InstanceUser>(instanceUser, Mapper);
            });
        }
    }
}
