﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public abstract class RepositoryBase
    {
        private readonly IMapper _mapper;
        private readonly IUnitOfWorkFactory _unitOfWorkFactory;
        public RepositoryBase(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
        {
            _unitOfWorkFactory = unitOfWorkFactory;
            _mapper = mapper;
        }
        protected IUnitOfWorkFactory UnitOfWorkFactory
        {
            get
            {
                return _unitOfWorkFactory;
            }
        }

        protected IMapper Mapper
        {
            get
            {
                return _mapper;
            }
        }

        protected static Expression<Func<TData,TKey>> DomainOrderToDataOrder<TData,TDomain,TKey>(Expression<Func<TDomain, TKey>> domainOrder)
        {
            LambdaExpression domainLambda = domainOrder as LambdaExpression;
            MemberExpression domainMemberAccess = domainLambda.Body as MemberExpression;
            MemberInfo domainMemberInfo = domainMemberAccess.Member;

            var dataParameter = Expression.Parameter(typeof(TData));
            var dataMemberInfo = typeof(TData).GetMember(domainMemberInfo.Name)[0];
            var dataMemberAccess = Expression.MakeMemberAccess(dataParameter, dataMemberInfo);
            Expression<Func<TData, TKey>> dataLambda = Expression.Lambda<Func<TData, TKey>>(dataMemberAccess, dataParameter);

            return dataLambda;
        }
    }
}
