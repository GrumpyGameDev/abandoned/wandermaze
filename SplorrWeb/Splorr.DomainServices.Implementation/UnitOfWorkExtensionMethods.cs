﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public static class UnitOfWorkExtensionMethods
    {
        public static TEntity StandardCreate<TEntity, TDataEntity>(this IUnitOfWork uow, TEntity entity, IMapper mapper) where TDataEntity:class
        {
            TDataEntity dataEntity = mapper.Map<TDataEntity>(entity);
            uow.MarkNew(dataEntity);
            uow.Commit();
            entity = mapper.Map<TEntity>(dataEntity);
            return entity;
        }
    }
}
