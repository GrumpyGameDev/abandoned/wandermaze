﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper;

namespace Splorr.DomainServices
{
    public static class DataToDomainMapping
    {
        public static void CreateMaps(IMapperConfigurationExpression cfg)
        {
            cfg.CreateMap<Instance, Domain.Instance>()
                .ForMember(d=>d.InstanceType,o=>o.MapFrom(s=>(Domain.InstanceType)s.InstanceTypeId));
            cfg.CreateMap<InstanceUser, Domain.InstanceUser>()
                .ForMember(d=>d.Level, o=>o.MapFrom(s=>(Domain.InstanceUserLevel)s.InstanceUserLevelId));
            cfg.CreateMap<UserInstance, Domain.UserInstance>()
                .ForMember(d=>d.Type,o=>o.MapFrom(s=>(Domain.InstanceType)s.Type));
        }
    }
}
