﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr.Domain;
using AutoMapper;

namespace Splorr.DomainServices
{
    public class InstanceRepository : RepositoryBase, IInstanceRepository
    {
        public InstanceRepository(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
            : base(unitOfWorkFactory, mapper)
        {
        }
        public Domain.Instance Create(Domain.Instance instance)
        {
            if(instance==null)
            {
                throw new ArgumentNullException("instance");
            }

            return UnitOfWorkFactory.WithUnitOfWork(uow => 
            {
                return uow.StandardCreate<Domain.Instance, Instance>(instance, Mapper);
            });

        }

        public bool IsNameUnique(string instanceName, int? instanceId)
        {
            return UnitOfWorkFactory.WithUnitOfWork(uow => 
            {
                if(instanceId.HasValue)
                {
                    int actualInstanceId = instanceId.Value;
                    return uow.Exists<Instance>(x => x.InstanceId != actualInstanceId && x.InstanceName == instanceName);
                }
                else
                {
                    return uow.Exists<Instance>(x => x.InstanceName == instanceName);
                }
            });
        }
    }
}
