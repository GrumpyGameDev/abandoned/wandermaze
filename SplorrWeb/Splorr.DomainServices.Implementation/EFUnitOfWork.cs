﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    [ExcludeFromCodeCoverage]
    public class EFUnitOfWork : IUnitOfWork
    {
        private SplorrEntities _context;
        public EFUnitOfWork(SplorrEntities context)
        {
            _context = context;
        }

        public void Commit()
        {
            _context.SaveChanges();
        }

        public int Count<TDataEntity>(Expression<Func<TDataEntity, bool>> filter) where TDataEntity:class
        {
            return _context.Set<TDataEntity>().Count(filter);
        }

        public bool Exists<TDataEntity>(Expression<Func<TDataEntity, bool>> filter) where TDataEntity : class
        {
            return _context.Set<TDataEntity>().Any(filter);
        }

        public IEnumerable<TDataEntity> Find<TDataEntity,TKey>(int skip, int take, Expression<Func<TDataEntity, bool>> filter, Expression<Func<TDataEntity, TKey>> order, bool descending) where TDataEntity : class
        {
            var filtered = _context.Set<TDataEntity>().Where(filter);
            if (descending)
            {
                return filtered.OrderByDescending(order).Skip(skip).Take(take).ToList();
            }
            else
            {
                return filtered.OrderBy(order).Skip(skip).Take(take).ToList();
            }
        }

        public void MarkDeleted<TDataEntity>(TDataEntity entity) where TDataEntity : class
        {
            throw new NotImplementedException();
        }

        public void MarkDirty<TDataEntity>(TDataEntity entity) where TDataEntity : class
        {
            throw new NotImplementedException();
        }

        public void MarkNew<TDataEntity>(TDataEntity entity) where TDataEntity : class
        {
            throw new NotImplementedException();
        }
    }
}
