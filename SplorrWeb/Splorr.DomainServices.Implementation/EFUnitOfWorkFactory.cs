﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    [ExcludeFromCodeCoverage]
    public class EFUnitOfWorkFactory : IUnitOfWorkFactory
    {
        public void WithUnitOfWork(Action<IUnitOfWork> action)
        {
            WithUnitOfWork(uow => { action(uow);  return true; });
        }

        public TResult WithUnitOfWork<TResult>(Func<IUnitOfWork, TResult> func)
        {
            using (SplorrEntities entities = new SplorrEntities())
            {
                return func(new EFUnitOfWork(entities));
            }
        }
    }
}
