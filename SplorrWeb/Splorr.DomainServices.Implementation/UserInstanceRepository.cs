﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Splorr.Domain;
using AutoMapper;
using System.Reflection;

namespace Splorr.DomainServices
{
    public class UserInstanceRepository : RepositoryBase, IUserInstanceRepository
    {
        private IMapper _mapper;

        public UserInstanceRepository(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
            :base(unitOfWorkFactory, mapper)
        {
            _mapper = mapper;
        }

        private static Expression<Func<DomainServices.UserInstance, bool>> GetFilterExpression(string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool? closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId)
        {
            bool filterName = !string.IsNullOrEmpty(instanceNameFilter);
            bool filterType = typeFilter.HasValue;
            bool filterPublic = publicFilter.HasValue;
            bool filterClosed = closedFilter.HasValue;
            bool filterLocked = lockedFilter.HasValue;
            bool filterLevel = levelFilter.HasValue;

            int actualTypeFilter = (int)(typeFilter ?? InstanceType.None);
            bool actualPublicFilter = publicFilter ?? false;
            bool actualClosedFilter = closedFilter ?? false;
            bool actualLockedFilter = lockedFilter ?? false;
            int actualLevelFilter = (int)(levelFilter ?? InstanceUserLevel.None);

            return x =>
                (!filterName || x.InstanceName == instanceNameFilter) &&
                (!filterType || x.Type == actualTypeFilter) &&
                (!filterPublic || x.Public == actualPublicFilter) &&
                (!filterClosed || x.Closed == actualClosedFilter) &&
                (!filterLocked || x.Locked == actualLockedFilter) &&
                (!filterLevel || x.Level == actualLevelFilter) &&
                x.UserId == userId;
        }

        public int Count(string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool? closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId)
        {
            return UnitOfWorkFactory.WithUnitOfWork(uow => 
            uow.Count(GetFilterExpression(
                instanceNameFilter,
                typeFilter,
                publicFilter,
                closedFilter,
                lockedFilter,
                levelFilter,
                userId)));
        }

        public IEnumerable<Domain.UserInstance> Find<TKey>(int skip, int take, string instanceNameFilter, InstanceType? typeFilter, bool? publicFilter, bool? closedFilter, bool? lockedFilter, InstanceUserLevel? levelFilter, string userId, Expression<Func<Domain.UserInstance, TKey>> order, bool descending)
        {
            return
                _mapper.Map<IEnumerable<Domain.UserInstance>>(
                    UnitOfWorkFactory.WithUnitOfWork(uow =>
                    uow.Find<UserInstance, TKey>(skip, take, GetFilterExpression(
                        instanceNameFilter,
                        typeFilter,
                        publicFilter,
                        closedFilter,
                        lockedFilter,
                        levelFilter,
                        userId),
                        DomainOrderToDataOrder<UserInstance,Domain.UserInstance,TKey>(order),
                        descending)));

        }
    }
}
