﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr.DomainServices
{
    public class UserRepository : RepositoryBase, IUserRepository
    {
        public UserRepository(IUnitOfWorkFactory unitOfWorkFactory, IMapper mapper)
            :base(unitOfWorkFactory, mapper)
        {
        }
        public bool IsAdmin(string userId)
        {
            return UnitOfWorkFactory.WithUnitOfWork(uow => 
                uow.Exists<AspNetUserRole>(r => 
                    r.UserId == userId && r.RoleId == Domain.RoleIds.Admin));
        }

        public bool IsUser(string userId)
        {
            return UnitOfWorkFactory.WithUnitOfWork(uow => 
                uow.Exists<AspNetUserRole>(r => 
                    r.UserId == userId && r.RoleId == Domain.RoleIds.User));
        }
    }
}
