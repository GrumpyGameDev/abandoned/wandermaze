#include "SDL.h"
#include <stdio.h>

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 720
#define WORLD_WIDTH 720
#define WORLD_HEIGHT 720
#define WORLD_X 0
#define WORLD_Y 0

typedef enum {BLACK, BLUE, GREEN, CYAN, RED, MAGENTA, BROWN, LIGHT_GRAY, DARK_GRAY, LIGHT_BLUE, LIGHT_GREEN, LIGHT_CYAN, LIGHT_MAGENTA, LIGHT_YELLOW, WHITE} color_type;

SDL_Color palette[] = 
{
	{ 0,0,0,255 },
	{ 0,0,170,255 },
	{ 0,170,0,255 },
	{ 0,170,170,255 },
	{ 170,0,0,255 },
	{ 170,0,170,255 },
	{ 170,85,0,255 },
	{ 170,170,170,255 },
	{ 85,85,85,255 },
	{ 85,85,255,255 },
	{ 85,255,0,255 },
	{ 85,255,255,255 },
	{ 255,85,85,255 },
	{ 255,85,255,255 },
	{ 255,255,85,255 },
	{ 255,255,255,255 }
};

color_type world[WORLD_WIDTH * WORLD_HEIGHT] = { 0 };
int player_x = WORLD_WIDTH / 2;
int player_y = WORLD_HEIGHT / 2;

void SetRenderColor(SDL_Renderer* renderer, color_type color)
{
	SDL_SetRenderDrawColor(renderer, palette[color].r, palette[color].g, palette[color].b, palette[color].a);
}

int main(int argc, char** argv)
{
	SDL_Init(SDL_INIT_EVERYTHING);

	printf("Initialized SDL!");

	SDL_Window* window = 0;
	SDL_Renderer* renderer = 0;

	SDL_CreateWindowAndRenderer(640, 360, SDL_WINDOW_SHOWN, &window, &renderer);

	SetRenderColor(renderer, BLACK);
	SDL_RenderClear(renderer);
	SDL_Rect rc = {0,0,1,1};
	for (int x = 0; x < WORLD_WIDTH; ++x)
	{
		for (int y = 0; y < WORLD_HEIGHT; ++y)
		{
			rc.x = x;
			rc.y = y;
			SetRenderColor(renderer, world[x+y*WORLD_WIDTH]);
			SDL_RenderFillRect(renderer, &rc);
		}
	}
	SDL_RenderPresent(renderer);

	SDL_Event evt;
	for (;;)
	{
		SDL_WaitEvent(&evt);
		if (evt.type == SDL_QUIT)
		{
			break;
		}
	}

	SDL_DestroyRenderer(renderer);

	SDL_DestroyWindow(window);

	SDL_Quit();

	return 0;
}