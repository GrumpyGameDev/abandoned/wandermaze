﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Linq;

namespace SplorrClient
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Client : Game
    {
        private GraphicsDeviceManager _graphics;
        private SpriteBatch _spriteBatch;
        private GameColors _colors;
        private GamePatterns _patterns;
        private GameImageDescriptors _imageDescriptors;
        private Images _images;

        private ImageRenderer _imageRenderer;
        private GameController _gameController;

        private KeyboardState _previousKeyboardState;
        private Dictionary<PlayerIndex, GamePadState> _previousGamePadState = new Dictionary<PlayerIndex, GamePadState>();

        private static readonly PlayerIndex[] _players = new PlayerIndex[] { PlayerIndex.One, PlayerIndex.Two, PlayerIndex.Three, PlayerIndex.Four };
        private static readonly Buttons[] _buttons = new Buttons[] {
                                                Buttons.DPadUp,
                                                Buttons.DPadDown,
                                                Buttons.DPadLeft,
                                                Buttons.DPadRight,
                                                Buttons.Start,
                                                Buttons.Back,
                                                Buttons.LeftStick,
                                                Buttons.RightStick,
                                                Buttons.LeftShoulder,
                                                Buttons.RightShoulder,
                                                Buttons.BigButton,
                                                Buttons.A,
                                                Buttons.B,
                                                Buttons.X,
                                                Buttons.Y,
                                                Buttons.LeftThumbstickLeft,
                                                Buttons.RightTrigger,
                                                Buttons.LeftTrigger,
                                                Buttons.RightThumbstickUp,
                                                Buttons.RightThumbstickDown,
                                                Buttons.RightThumbstickRight,
                                                Buttons.RightThumbstickLeft,
                                                Buttons.LeftThumbstickUp,
                                                Buttons.LeftThumbstickDown,
                                                Buttons.LeftThumbstickRight};

        public Client()
        {
            _graphics = new GraphicsDeviceManager(this);
            _graphics.PreferredBackBufferWidth = 800;
            _graphics.PreferredBackBufferHeight = 600;
            _graphics.ApplyChanges();
            Content.RootDirectory = "Content";
        }

        protected override void Initialize()
        {
            this.Window.Title = "Splorr!!";
            base.Initialize();
        }

        protected override void LoadContent()
        {
            _spriteBatch = new SpriteBatch(GraphicsDevice);
            _colors = new GameColors();
            _patterns = new GamePatterns();
            _imageDescriptors = new GameImageDescriptors();
            _images = new Images(GraphicsDevice, _colors, _patterns);
            _imageRenderer = new ImageRenderer(_imageDescriptors, _images, _spriteBatch);
            _gameController = new GameController(this,_imageRenderer);
            _previousKeyboardState = Keyboard.GetState();
            _players.ToList().ForEach(x => _previousGamePadState[x] = GamePad.GetState(x));
        }

        protected override void UnloadContent()
        {
        }

        protected override void Update(GameTime gameTime)
        {
            var currentKeyboardState = Keyboard.GetState();

            var previousPressedKeys = _previousKeyboardState.GetPressedKeys();
            var currentPressedKeys = currentKeyboardState.GetPressedKeys();

            var releasedKeys = previousPressedKeys.Where(k => !currentPressedKeys.Contains(k)).ToList();
            var pressedKeys = currentPressedKeys.Where(k => !previousPressedKeys.Contains(k)).ToList();

            _previousKeyboardState = currentKeyboardState;

            releasedKeys.ForEach(x => 
                _gameController.DispatchInput(new KeyUpMessage(x))
            );
            pressedKeys.ForEach(x =>
                _gameController.DispatchInput(new KeyDownMessage(x))
            );

            _players.ToList().ForEach(player => 
            {
                var currentState = GamePad.GetState(player);
                var previousState = _previousGamePadState[player];

                if(currentState.IsConnected!=previousState.IsConnected)
                {
                    if(currentState.IsConnected)
                    {
                        _gameController.DispatchInput(new GamePadConnectMessage(player));
                    }
                    else
                    {
                        _gameController.DispatchInput(new GamePadDisconnectMessage(player));
                    }
                }

                _buttons.ToList().ForEach(button =>
                {
                    if(currentState.IsButtonDown(button)!=previousState.IsButtonDown(button))
                    {
                        if(currentState.IsButtonDown(button))
                        {
                            _gameController.DispatchInput(new GamePadButtonDownMessage(player,button));
                        }
                        else
                        {
                            _gameController.DispatchInput(new GamePadButtonUpMessage(player, button));
                        }
                    }
                });

                _previousGamePadState[player] = currentState;
            });

            base.Update(gameTime);

            _gameController.Notify(new UpdateMessage(gameTime));
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(_colors["black"].Value);

            _spriteBatch.Begin(SpriteSortMode.Deferred,null,SamplerState.PointClamp,null,null,null,Matrix.CreateScale(5.0f,5.0f,1.0f));

            _gameController.Notify(new DrawMessage(gameTime));

            _spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
