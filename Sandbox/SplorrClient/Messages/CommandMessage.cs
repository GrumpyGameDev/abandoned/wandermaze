﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class CommandMessage<T>:Message
    {
        private T _command;
        public T Command
        {
            get
            {
                return _command;
            }
        }
        public CommandMessage(int id, T command)
            :base(id)
        {
            _command = command;
        }
    }
}
