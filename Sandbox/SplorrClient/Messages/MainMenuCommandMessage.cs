﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class MainMenuCommandMessage:CommandMessage<MainMenuCommand>
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public MainMenuCommandMessage(MainMenuCommand command)
            :base(MessageId, command)
        {

        }
    }
}
