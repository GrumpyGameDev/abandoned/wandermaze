﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class ShowMainMenuMessage:Message
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public ShowMainMenuMessage()
            :base(MessageId)
        {

        }
    }
}
