﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class ClearNotificationMessage:Message
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;

        public ClearNotificationMessage()
            :base(MessageId)
        {

        }
    }
}
