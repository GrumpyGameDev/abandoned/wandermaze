﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class UICommandMessage:CommandMessage<UICommand>
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public UICommandMessage(UICommand command)
            :base(MessageId, command)
        {

        }
    }
}
