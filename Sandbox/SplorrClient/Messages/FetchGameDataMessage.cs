﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr;

namespace SplorrClient
{
    public class FetchGameDataMessage: ActionMessage<GameData>
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        private GameData _gameData;

        public GameData GameData
        {
            get
            {
                return _gameData;
            }
        }

        public FetchGameDataMessage()
            :base(MessageId, x=> { })
        {
            Action = x => _gameData = x;
        }
    }
}
