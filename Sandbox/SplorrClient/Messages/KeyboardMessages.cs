﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class KeyboardMessage: Message
    {
        private Keys _key;
        public Keys Key
        {
            get
            {
                return _key;
            }
        }

        public KeyboardMessage(int id, Keys key)
            :base(id)
        {
            _key = key;
        }

    }
    public class KeyDownMessage: KeyboardMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public KeyDownMessage(Keys key)
            :base(MessageId, key)
        {

        }
    }
    public class KeyUpMessage: KeyboardMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public KeyUpMessage(Keys key)
            :base(MessageId, key)
        {
        }
    }
}
