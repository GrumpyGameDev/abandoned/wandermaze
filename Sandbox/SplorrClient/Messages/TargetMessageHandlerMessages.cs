﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public abstract class MessageHandlerTargetMessage:Message
    {
        private MessageHandler _target;
        public MessageHandler Target
        {
            get
            {
                return _target;
            }
        }

        public MessageHandlerTargetMessage(int id, MessageHandler target)
            :base(id)
        {
            _target = target;
        }
    }
    public class SetInputFocusMessage: MessageHandlerTargetMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;

        public SetInputFocusMessage(MessageHandler target)
            :base(MessageId, target)
        {

        }
    }
    public class CancelMenuMessage: MessageHandlerTargetMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public CancelMenuMessage(MessageHandler target)
            :base(MessageId, target)
        {

        }
    }
}
