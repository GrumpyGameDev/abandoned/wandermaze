﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public abstract class ActionMessage<T>:Message
    {
        private Action<T> _action;
        public Action<T> Action
        {
            get
            {
                return _action;
            }
            protected set
            {
                _action = value;
            }
        }
        public ActionMessage(int id, Action<T> action)
            :base(id)
        {
            _action = action;
        }
    }
}
