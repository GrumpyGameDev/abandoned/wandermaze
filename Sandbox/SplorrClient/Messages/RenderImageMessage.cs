﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public struct RenderImagePayload
    {
        public string ImageName;
        public int X;
        public int Y;
        public RenderImagePayload(string imageName, int x, int y)
        {
            ImageName = imageName;
            X = x;
            Y = y;
        }
    }
    public class RenderImageMessage:Message
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        private RenderImagePayload _payload;
        public RenderImagePayload Payload
        {
            get
            {
                return _payload;
            }
        }
        public RenderImageMessage(RenderImagePayload payload)
            :base(MessageId)
        {
            _payload = payload;
        }
        public RenderImageMessage(string imageName, int x, int y)
            :this(new RenderImagePayload(imageName,x,y))
        {

        }
    }
}
