﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public struct RenderMapPayload
    {
        public Splorr.Map Map;
        public int X;
        public int Y;
        public int Columns;
        public int Rows;
        public RenderMapPayload(Splorr.Map map, int x, int y,int columns,int rows)
        {
            Map = map;
            X = x;
            Y = y;
            Columns = columns;
            Rows = rows;
        }
    }
    public class RenderMapMessage:Message
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        private RenderMapPayload _payload;
        public RenderMapPayload Payload
        {
            get
            {
                return _payload;
            }
        }
        public RenderMapMessage(RenderMapPayload payload)
            :base(MessageId)
        {
            _payload = payload;
        }
        public RenderMapMessage(Splorr.Map map, int x, int y, int columns, int rows)
            : this(new RenderMapPayload(map, x, y, columns, rows))
        {

        }
    }
}
