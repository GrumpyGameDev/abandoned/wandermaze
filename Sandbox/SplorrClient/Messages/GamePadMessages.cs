﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public abstract class GamePadMessage:Message
    {
        private PlayerIndex _player;
        public PlayerIndex Player
        {
            get
            {
                return _player;
            }
        }
        public GamePadMessage(int id, PlayerIndex player)
            :base(id)
        {
            _player = player;
        }
    }
    public class GamePadConnectMessage : GamePadMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public GamePadConnectMessage(PlayerIndex player)
            : base(MessageId, player)
        {

        }
    }
    public class GamePadDisconnectMessage : GamePadMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public GamePadDisconnectMessage(PlayerIndex player)
            : base(MessageId, player)
        {

        }
    }
    public abstract class GamePadButtonMessage : GamePadMessage
    {
        private Buttons _button;
        public Buttons Button
        {
            get
            {
                return _button;
            }
        }
        public GamePadButtonMessage(int id, PlayerIndex player, Buttons button)
            : base(id, player)
        {
            _button = button;
        }
    }
    public class GamePadButtonDownMessage : GamePadButtonMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public GamePadButtonDownMessage(PlayerIndex player, Buttons button)
            : base(MessageId, player, button)
        {

        }

    }
    public class GamePadButtonUpMessage : GamePadButtonMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public GamePadButtonUpMessage(PlayerIndex player, Buttons button)
            : base(MessageId, player, button)
        {

        }

    }
}
