﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr;

namespace SplorrClient
{
    public class FetchNotificationMessage: ActionMessage<string>
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        private string _notification;

        public string Notification
        {
            get
            {
                return _notification;
            }
        }

        public FetchNotificationMessage()
            :base(MessageId, x=> { })
        {
            Action = x => _notification = x;
        }
    }
}
