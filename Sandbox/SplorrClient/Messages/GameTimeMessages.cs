﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public abstract class GameTimeMessage: Message
    {
        private GameTime _gameTime;
        public GameTime GameTime
        {
            get
            {
                return _gameTime;
            }
        }

        public GameTimeMessage(int id, GameTime gameTime)
            :base(id)
        {
            _gameTime = gameTime;
        }

    }
    public class UpdateMessage:GameTimeMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public UpdateMessage(GameTime gameTime)
            :base(MessageId, gameTime)
        {

        }
    }
    public class DrawMessage:GameTimeMessage
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        public DrawMessage(GameTime gameTime)
            :base(MessageId, gameTime)
        {
        }
    }
}
