﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public struct RenderTextPayload
    {
        public string Text;
        public int X;
        public int Y;
        public string Foreground;
        public string Background;
        public RenderTextPayload(string text, int x, int y,string foreground,string background)
        {
            Text = text;
            X = x;
            Y = y;
            Foreground = foreground;
            Background = background;
        }
    }
    public class RenderTextMessage:Message
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;
        private RenderTextPayload _payload;
        public RenderTextPayload Payload
        {
            get
            {
                return _payload;
            }
        }
        public RenderTextMessage(RenderTextPayload payload)
            :base(MessageId)
        {
            _payload = payload;
        }
        public RenderTextMessage(string text, int x, int y, string foreground, string background)
            :this(new RenderTextPayload(text,x,y, foreground, background))
        {

        }
    }
}
