﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class SetScaleMessage:Message
    {
        public static readonly int MessageId = SplorrClient.MessageId.NextMessageId;

        public float XScale { get; private set; }
        public float YScale { get; private set; }
        public float ZScale { get; private set; }

        public SetScaleMessage(float xScale, float yScale, float zScale)
            :base(MessageId)
        {
            XScale = xScale;
            YScale = yScale;
            ZScale = zScale;
        }
    }
}
