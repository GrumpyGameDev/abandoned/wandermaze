﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public struct Pattern
    {
        public uint[] Data;
        public int Columns;

        public static uint[] MakeData(params uint[] data)
        {
            return data;
        }

        public static uint[] MakeData(params string[] data)
        {
            return data.Select(s => 
            {
                uint result = 0;
                uint flag = 1;

                s.ToCharArray().ToList().ForEach(c => 
                {
                    if(c=='X')
                    {
                        result += flag;
                    }
                    flag *= 2;
                });

                return result;
            }).ToArray();
        }

        public Pattern(uint[] data, int columns)
        {
            Data = data;
            Columns = columns;
        }
    }
}
