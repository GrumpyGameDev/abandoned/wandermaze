﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public struct ImageDescriptor
    {
        public string Pattern;
        public string Foreground;
        public string Background;

        public ImageDescriptor(string pattern, string foreground, string background)
        {
            Pattern = pattern;
            Foreground = foreground;
            Background = background;
        }
    }
}
