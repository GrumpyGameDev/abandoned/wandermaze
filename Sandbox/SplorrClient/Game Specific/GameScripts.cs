﻿using Splorr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class GameScripts: IScriptTable
    {
        internal static class Flags
        {
            internal const string Spawned = "spawned";
            internal const string Bump = "bump";
        }

        internal static class Notifications
        {
            internal const string TagonSpawned = "tagon-spawned";
            internal const string TagonBumped = "tagon-bumped";
            internal const string KeyBumped = "key-bumped";
            internal const string KeyPickup = "key-pickup";
        }

        internal static class Scripts
        {
            internal const string CreateTree = "create-tree";
            internal const string CreateGrass = "create-grass";
            internal const string CanEnterTree = "can-enter-tree";
            internal const string CanEnterGrass = "can-enter-grass";
            internal const string CreateTagon = "create-tagon";
            internal const string SpawnTagon = "spawn-tagon";
            internal const string CreateKey = "create-key";
            internal const string BumpTagon = "bump-tagon";
            internal const string BumpKey = "bump-key";
            internal const string Allow = "allow";
            internal const string Deny = "deny";
            internal const string CanPickupKey = "can-pickup-key";
            internal const string PickupKey = "pickup-key";
        }


        private Dictionary<string, Func<InstanceBase, Descriptor, InstanceBase, Descriptor, MapCell, object>> _scripts = new Dictionary<string, Func<InstanceBase, Descriptor, InstanceBase, Descriptor, MapCell, object>>();

        private object DefaultAction(InstanceBase selfInstance, Descriptor selfDescriptor, InstanceBase otherInstance, Descriptor otherDescriptor, MapCell where)
        {
            throw new NotImplementedException();
        }

        public Func<InstanceBase, Descriptor, InstanceBase, Descriptor, MapCell, object> this[string index]
        {
            get
            {
                if(_scripts.ContainsKey(index))
                {
                    return _scripts[index];
                }
                else
                {
                    return DefaultAction;
                }
            }
            set
            {
                if(value==null)
                {
                    if(_scripts.ContainsKey(index))
                    {
                        _scripts.Remove(index);
                    }
                }
                else
                {
                    _scripts[index] = value;
                }
            }
        }

        public GameScripts()
        {
            #region Common Scripts

            this[Scripts.Allow] = (s, sd, o, od, w) => true;
            this[Scripts.Deny] = (s, sd, o, od, w) => false;

            #endregion

            #region Tree

            this[Scripts.CreateTree] = (s, sd, o, od, w) =>
            {
                s.SetStringProperty(Splorr.Properties.ImageName, "tree");
                s.SetScript(Splorr.Scripts.CanEnter, Scripts.Deny);
                return true;
            };
            this[Scripts.CanEnterTree] = (s, sd, o, od, w) =>
            {
                o.RunScript(Scripts.BumpTagon, o, od, s, sd, w);
                return false;
            };

            #endregion

            #region Grass

            this[Scripts.CreateGrass] = (s, sd, o, od, w) =>
            {
                s.SetScript(Splorr.Scripts.OnEnter, Scripts.Allow);
                s.SetStringProperty(Splorr.Properties.ImageName, "grass");
                s.SetBoolProperty(Splorr.Scripts.CanEnter, true);
                return true;
            };

            #endregion

            #region Tagon

            this[Scripts.CreateTagon] = (s, sd, o, od, w) =>
            {
                s.SetStringProperty(Splorr.Properties.ImageName, "tagon");
                s.SetBoolProperty(Flags.Spawned, false);
                s.SetBoolProperty(Flags.Bump, false);
                s.SetScript(Scripts.BumpTagon, Scripts.BumpTagon);
                s.SetScript(Splorr.Scripts.OnEnter, Scripts.Allow);
                return true;
            };
            this[Scripts.SpawnTagon] = (s, sd, o, od, w) =>
            {
                if (!(s.GetBoolProperty(Flags.Spawned) ?? false))
                {
                    (s as CreatureInstance)?.Notify(Notifications.TagonSpawned);
                    s.SetBoolProperty(Flags.Spawned, true);
                }
                return true;
            };
            this[Scripts.BumpTagon] = (s, sd, o, od, w) =>
            {
                if (!(s.GetBoolProperty(Flags.Bump) ?? false))
                {
                    (s as CreatureInstance)?.Notify(Notifications.TagonBumped);
                    s.SetBoolProperty(Flags.Bump, true);
                }
                return true;
            };

            #endregion

            #region Key

            this[Scripts.CreateKey] = (s, sd, o, od, w) =>
            {
                s.SetStringProperty(Splorr.Properties.ImageName, "key");
                s.SetScript(Splorr.Scripts.CanPickup, Scripts.CanPickupKey);
                s.SetScript(Splorr.Scripts.OnPickup, Scripts.PickupKey);
                return true;
            };
            this[Scripts.CanPickupKey] = (s, sd, o, od, w) =>
            {
                if (!(s.GetBoolProperty(Flags.Bump) ?? false))
                {
                    (o as CreatureInstance)?.Notify(Notifications.KeyBumped);
                    s.SetBoolProperty(Flags.Bump, true);
                    return false;
                }
                return true;
            };
            this[Scripts.PickupKey] = (s, sd, o, od, w) =>
            {
                (o as CreatureInstance)?.Notify(Notifications.KeyPickup);
                return true;
            };

            #endregion

        }
    }
}
