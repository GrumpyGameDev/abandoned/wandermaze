﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class GameImageDescriptors: StructTable<string, ImageDescriptor>
    {
        public GameImageDescriptors()
        {
            this["grass"] = new ImageDescriptor("field", "darkjade", "black");
            this["water"] = new ImageDescriptor("field", "ruby", "darkruby");
            this["path"] = new ImageDescriptor("field", "black", "darksilver");
            this["bush"] = new ImageDescriptor("bush", "darkjade", "black");
            this["tree"] = new ImageDescriptor("tree", "jade", "black");
            this["choppabletree"] = new ImageDescriptor("tree", "mediumjade", "black");
            this["house"] = new ImageDescriptor("house", "carnelian", "black");
            this["empty"] = new ImageDescriptor("empty", "black", "black");
            this["tagon"] = new ImageDescriptor("tagon", "medium", "transparent");
            this["cursor"] = new ImageDescriptor("cursor", "amethyst", "transparent");
            this["none"] = new ImageDescriptor("empty", "transparent", "transparent");
            this["unexplored"] = new ImageDescriptor("character63", "black", "gold");
            this["key"] = new ImageDescriptor("key", "carnelian", "transparent");
        }
    }
}
