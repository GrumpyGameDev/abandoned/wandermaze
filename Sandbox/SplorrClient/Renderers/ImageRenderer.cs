﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class ImageRenderer
    {
        private StructTable<string, ImageDescriptor> _imageDescriptors;
        private Images _images;
        private SpriteBatch _spriteBatch;

        public ImageRenderer(
            StructTable<string, ImageDescriptor> imageDescriptors,
            Images images,
            SpriteBatch spriteBatch)
        {
            _imageDescriptors = imageDescriptors;
            _images = images;
            _spriteBatch = spriteBatch;
        }

        public void Render(string index, int x, int y)
        {
            var texture = _images[_imageDescriptors[index].Value];

            _spriteBatch.Draw(texture, new Rectangle(x, y, texture.Width, texture.Height), Color.White);
        }

        public void RenderText(string text, int x, int y, string foreground, string background)
        {
            ImageDescriptor descriptor = new ImageDescriptor(null, foreground, background);

            Encoding.ASCII.GetBytes(text)
                .Select(c=>string.Format("character{0}",c))
                .ToList()
                .ForEach(imageName=> 
                {
                    descriptor.Pattern = imageName;
                    var texture = _images[descriptor];

                    _spriteBatch.Draw(texture, new Rectangle(x, y, texture.Width, texture.Height), Color.White);

                    x += texture.Width;
                });
        }
        public void RenderMap(Splorr.Map map, int x, int y, int columns, int rows)
        {
            int screenX = x;
            for(int column=0;column< columns;++column)
            {
                int width = 0;
                int screenY = y;
                for(int row=0;row< rows;++row)
                {
                    var mapCell = map.GetMapCell(column, row);

                    var descriptor = _imageDescriptors[mapCell.TerrainInstance.ImageName];

                    var texture = _images[descriptor.Value];

                    width = Math.Max(width, texture.Width);
                    int height = texture.Height;

                    _spriteBatch.Draw(texture, new Rectangle(screenX, screenY, texture.Width, texture.Height), Color.White);

                    var creatureInstance = mapCell.CreatureInstance;
                    if(creatureInstance!= null)
                    {
                        texture = _images[_imageDescriptors[creatureInstance.ImageName].Value];
                        _spriteBatch.Draw(texture, new Rectangle(screenX, screenY, texture.Width, texture.Height), Color.White);
                    }

                    var effectInstance = mapCell.EffectInstance;
                    if (effectInstance != null)
                    {
                        texture = _images[_imageDescriptors[effectInstance.ImageName].Value];
                        _spriteBatch.Draw(texture, new Rectangle(screenX, screenY, texture.Width, texture.Height), Color.White);
                    }

                    var itemInstance = mapCell.PeekItem();
                    if(itemInstance!= null)
                    {
                        texture = _images[_imageDescriptors[itemInstance.ImageName].Value];
                        _spriteBatch.Draw(texture, new Rectangle(screenX, screenY, texture.Width, texture.Height), Color.White);
                    }

                    screenY += height;
                }
                screenX += width;
            }
        }


        public void SetScale(float xScale, float yScale, float zScale)
        {
            _spriteBatch.End();
            _spriteBatch.Begin(SpriteSortMode.Deferred, null, SamplerState.PointClamp, null, null, null, Matrix.CreateScale(xScale, yScale, 1.0f));
        }
    }
}
