﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class StructTable<K,T> where T : struct
    {
        private Dictionary<K, T> _table = new Dictionary<K, T>();
        public T? this[K index]
        {
            get
            {
                T result;
                if (_table.TryGetValue(index, out result))
                {
                    return result;
                }
                else
                {
                    return null;
                }
            }
            set
            {
                if (value == null)
                {
                    if (_table.ContainsKey(index))
                    {
                        _table.Remove(index);
                    }
                }
                else
                {
                    _table[index] = value.Value;
                }
            }
        }
    }
}
