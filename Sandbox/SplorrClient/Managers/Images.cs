﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class Images
    {
        private GraphicsDevice _graphicsDevice;
        private StructTable<string, Color> _colorTable;
        private StructTable<string, Pattern> _patternTable;
        private Dictionary<ImageDescriptor, Texture2D> _imageTable = new Dictionary<ImageDescriptor, Texture2D>();

        public Texture2D this[ImageDescriptor index]
        {
            get
            {
                if(!_imageTable.ContainsKey(index))
                {
                    Pattern pattern = _patternTable[index.Pattern].Value;
                    Color foreground = _colorTable[index.Foreground].Value;
                    Color background = _colorTable[index.Background].Value;

                    Texture2D texture = new Texture2D(_graphicsDevice, pattern.Columns, pattern.Data.Length);

                    Color[] data = new Color[pattern.Columns * pattern.Data.Length];
                    for(int y=0;y<pattern.Data.Length;++y)
                    {
                        for(int x=0;x<pattern.Columns;++x)
                        {
                            data[x + y * pattern.Columns] = ((pattern.Data[y] & ((uint)(1 << x))) > 0) ? foreground : background;
                        }
                    }
                    texture.SetData(data);

                    _imageTable[index] = texture;
                }
                return _imageTable[index];
            }
        }

        public Images(GraphicsDevice graphicsDevice, StructTable<string,Color> colorTable, StructTable<string,Pattern> patternTable)
        {
            _graphicsDevice = graphicsDevice;
            _colorTable = colorTable;
            _patternTable = patternTable;
        }
    }
}
