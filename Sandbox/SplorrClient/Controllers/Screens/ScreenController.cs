﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public abstract class ScreenController : MessageHandler
    {
        public ScreenController(MessageHandler parent, bool disabled)
            :base(parent, disabled)
        {
        }
        protected Splorr.GameData GameData
        {
            get
            {
                var message = new FetchGameDataMessage();
                HandleMessage(message);
                return message.GameData;
            }
        }

        public override bool OnMessage(Message message)
        {
            if (message.Id == DrawMessage.MessageId)
            {
                return OnDrawMessage(message as DrawMessage);
            }
            else if (message.Id == UICommandMessage.MessageId)
            {
                return OnUICommandMessage(message as UICommandMessage);
            }
            else
            {
                return false;
            }
        }

        internal abstract bool OnUICommandMessage(UICommandMessage uICommandMessage);
        internal abstract bool OnDrawMessage(DrawMessage drawMessage);
    }
}
