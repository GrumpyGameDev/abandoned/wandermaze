﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class PlayScreenController : ScreenController
    {
        public PlayScreenController(MessageHandler parent, bool disabled)
            :base(parent, disabled)
        {
        }

        internal override bool OnDrawMessage(DrawMessage drawMessage)
        {
            var player = GameData.Player;
            var location = player.Location;
            var map = GameData.World.GetMap(location.Atlas, location.AtlasColumn, location.AtlasRow, location.Map);
            HandleMessage(new RenderMapMessage(map, 0, 0, 15, 15));
            return true;
        }

        internal override bool OnUICommandMessage(UICommandMessage uICommandMessage)
        {
            if(uICommandMessage== null)
            {
                throw new ArgumentNullException("uICommandMessage");
            }
            switch(uICommandMessage.Command)
            {
                case UICommand.MoveUp:
                    Move("north");
                    return true;
                case UICommand.MoveDown:
                    Move("south");
                    return true;
                case UICommand.MoveLeft:
                    Move("west");
                    return true;
                case UICommand.MoveRight:
                    Move("east");
                    return true;
                case UICommand.Start:
                    return HandleMessage(new ShowMainMenuMessage());
                default:
                    return false;
            }
        }

        private void Move(string direction)
        {
            GameData.Player.Move(direction);

        }
    }
}
