﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class NotificationScreenController : ScreenController
    {
        //private Dictionary<string, IEnumerable<RenderTextMessage>> _renderTextDictionary = new Dictionary<string, IEnumerable<RenderTextMessage>>();
        private Splorr.Dialog _dialog = null;
        private List<RenderTextMessage> _textMessages = new List<RenderTextMessage>();
        private List<RenderImageMessage> _imageMessages = new List<RenderImageMessage>();

        public NotificationScreenController(MessageHandler parent, bool disabled)
            :base(parent, disabled)
        {
            //_renderTextDictionary[GameScripts.Notifications.TagonSpawned] = new RenderTextMessage[] 
            //{
            //    new RenderTextMessage("Welcome to     ",60, 80,"silver","ruby"),
            //    new RenderTextMessage("Splorr!!       ",60, 88,"silver","ruby"),
            //    new RenderTextMessage("WASD=Move      ",60, 96,"black","darksilver"),
            //    new RenderTextMessage("Space=Action   ",60,104,"black","darksilver"),
            //    new RenderTextMessage("E=Interact     ",60,112,"black","darksilver"),
            //    new RenderTextMessage("X=Switch Hand  ",60,120,"black","darksilver"),
            //    new RenderTextMessage("Tab=Inventory  ",60,128,"black","darksilver"),
            //    new RenderTextMessage("Esc=Menu       ",60,136,"black","darksilver"),
            //    new RenderTextMessage("LShift=cancel  ",60,144,"black","darksilver"),
            //    new RenderTextMessage("----Space=Close",60,152,"black","jade")
            //};
            //_renderTextDictionary[GameScripts.Notifications.TagonBumped] = new RenderTextMessage[]
            //{
            //    new RenderTextMessage("Blocked!      ",8*15-4*14,8*15-4*4+8*0,"silver","ruby"),
            //    new RenderTextMessage("Some terrain  ",8*15-4*14,8*15-4*4+8*1,"black","darksilver"),
            //    new RenderTextMessage("is impassible.",8*15-4*14,8*15-4*4+8*2,"black","darksilver"),
            //    new RenderTextMessage("---Space=Close",8*15-4*14,8*15-4*4+8*3,"black","jade")
            //};
            //_renderTextDictionary[GameScripts.Notifications.KeyBumped] = new RenderTextMessage[]
            //{
            //    new RenderTextMessage("Useful Items!      ",8*15-4*19,8*15-4*4+8*0,"silver","ruby"),
            //    new RenderTextMessage("Pick up by moving  ",8*15-4*19,8*15-4*4+8*1,"black","darksilver"),
            //    new RenderTextMessage("onto them.         ",8*15-4*19,8*15-4*4+8*2,"black","darksilver"),
            //    new RenderTextMessage("--------Space=Close",8*15-4*19,8*15-4*4+8*3,"black","jade")
            //};
            //_renderTextDictionary[GameScripts.Notifications.KeyPickup] = new RenderTextMessage[]
            //{
            //    new RenderTextMessage("A Red Key!          ",8*15-4*20,8*15-4*4+8*0,"silver","ruby"),
            //    new RenderTextMessage("Usually this means  ",8*15-4*20,8*15-4*4+8*1,"black","darksilver"),
            //    new RenderTextMessage("there is a red door.",8*15-4*20,8*15-4*4+8*2,"black","darksilver"),
            //    new RenderTextMessage("---------Space=Close",8*15-4*20,8*15-4*4+8*3,"black","jade")
            //};
        }

        public override bool OnMessage(Message message)
        {
            if(message.Id == UpdateMessage.MessageId)
            {
                return OnUpdate(message as UpdateMessage);
            }
            else
            {
                return base.OnMessage(message);
            }
        }

        private bool OnUpdate(UpdateMessage updateMessage)
        {
            if(!Disabled && string.IsNullOrEmpty(CurrentNotification))
            {
                Disabled = true;
            }
            else
            {
                UpdateDialog();
            }
            return false;
        }

        private void UpdateDialog()
        {
            throw new NotImplementedException();
        }

        internal override bool OnDrawMessage(DrawMessage drawMessage)
        {
            HandleMessage(new SetScaleMessage(2.5f, 2.5f, 1.0f));
            //IEnumerable<RenderTextMessage> messages;
            //_renderTextDictionary.TryGetValue(CurrentNotification, out messages);
            foreach (var textMessage in _textMessages)
            {
                HandleMessage(textMessage);
            }
            foreach (var imageMessage in _imageMessages)
            {
                HandleMessage(imageMessage);
            }
            HandleMessage(new SetScaleMessage(5f, 5f, 1.0f));
            return true;
        }

        internal override bool OnUICommandMessage(UICommandMessage uICommandMessage)
        {
            if(uICommandMessage== null)
            {
                throw new ArgumentNullException("uICommandMessage");
            }

            switch(uICommandMessage.Command)
            {
                case UICommand.Confirm:
                case UICommand.Cancel:
                    HandleMessage(new ClearNotificationMessage());
                    return true;
                default:
                    return false;
            }
        }

        internal string CurrentNotification
        {
            get
            {
                FetchNotificationMessage message = new FetchNotificationMessage();
                HandleMessage(message);
                return message.Notification;
            }
        }
    }
}
