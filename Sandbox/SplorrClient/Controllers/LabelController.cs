﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public class LabelController : MessageHandler
    {
        public RenderTextPayload Text
        {
            get; set;
        }
        public LabelController(MessageHandler parent, bool disabled, RenderTextPayload text)
            :base(parent, disabled)
        {
            Text = text;
        }
        public override bool OnMessage(Message message)
        {
            if(message== null)
            {
                throw new ArgumentNullException("message");
            }
            if(message.Id == DrawMessage.MessageId)
            {
                return OnDrawMessage(message as DrawMessage);
            }
            else
            {
                return false;
            }
        }

        private bool OnDrawMessage(DrawMessage drawMessage)
        {
            HandleMessage(new RenderTextMessage(Text));
            return true;
        }
    }
}
