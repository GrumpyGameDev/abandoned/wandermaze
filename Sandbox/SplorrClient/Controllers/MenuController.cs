﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public struct ColorPair
    {
        public string Foreground;
        public string Background;
        public ColorPair(string foreground, string background)
        {
            Foreground = foreground;
            Background = background;
        }
    }
    public struct MenuHeaderDescriptor
    {
        public ColorPair Colors;
        public string Text;
        public MenuHeaderDescriptor(string text, string foreground,string background)
        {
            Text = text;
            Colors = new ColorPair(foreground, background);
        }
    }
    public struct MenuItemDescriptor<T>
    {
        public T Command { get; set; }
        public string Text { get; set; }
        public MenuItemDescriptor(string text,T command)
        {
            Text = text;
            Command = command;
        }
    }
    public struct MenuDescriptor<T>
    {
        public int X;
        public int Y;
        public int LineSpacing;
        public MenuHeaderDescriptor? MenuHeader;
        public ColorPair ItemColors;
        public int SelectedItem;
        public MenuItemDescriptor<T>[] Items;
        public MenuDescriptor<T> Next
        {
            get
            {
                SelectedItem++;
                SelectedItem %= Items.Length;
                return this;
            }
        }
        public MenuDescriptor<T> Previous
        {
            get
            {
                SelectedItem += (Items.Length - 1);
                SelectedItem %= Items.Length;
                return this;
            }
        }
    }
    public class MenuController<T>:MessageHandler
    {
        public MenuDescriptor<T> Descriptor { get; set; }
        public Func<MenuController<T>, MenuItemDescriptor<T>, bool> MenuActionHandler { get; set; }
        public MenuController(MessageHandler parent, bool disabled, MenuDescriptor<T> descriptor, Func<MenuController<T>, MenuItemDescriptor<T>, bool> menuActionHandler)
            :base(parent, disabled)
        {
            Descriptor = descriptor;
            MenuActionHandler = menuActionHandler;
        }

        public override bool OnMessage(Message message)
        {
            if(message.Id == DrawMessage.MessageId)
            {
                return OnDrawMessage(message as DrawMessage);
            }
            else if(message.Id == UICommandMessage.MessageId)
            {
                return OnUICommandMessage(message as UICommandMessage);
            }
            else
            {
                return false;
            }
        }

        private bool OnUICommandMessage(UICommandMessage uICommandMessage)
        {
            if(uICommandMessage== null)
            {
                throw new ArgumentNullException("uICommandMessage");
            }
            switch(uICommandMessage.Command)
            {
                case UICommand.MoveUp:
                case UICommand.MenuUp:
                case UICommand.LookUp:
                    Descriptor = Descriptor.Previous;
                    return true;
                case UICommand.MoveDown:
                case UICommand.MenuDown:
                case UICommand.LookDown:
                    Descriptor = Descriptor.Next;
                    return true;
                case UICommand.Confirm:
                    if (MenuActionHandler != null)
                    {
                        return MenuActionHandler(this, Descriptor.Items[Descriptor.SelectedItem]);
                    }
                    else
                    {
                        return false;
                    }
                case UICommand.Cancel:
                    return HandleMessage(new CancelMenuMessage(this));
                default:
                    return false;
            }
        }

        
        private bool OnDrawMessage(DrawMessage drawMessage)
        {
            int y = Descriptor.Y;
            if(Descriptor.MenuHeader.HasValue)
            {
                HandleMessage(new RenderTextMessage(Descriptor.MenuHeader.Value.Text, Descriptor.X, y, Descriptor.MenuHeader.Value.Colors.Foreground,Descriptor.MenuHeader.Value.Colors.Background));
                y += Descriptor.LineSpacing;
            }
            for(int index=0;index<Descriptor.Items.Length;++index)
            {
                var foreground = index == Descriptor.SelectedItem ? Descriptor.ItemColors.Background : Descriptor.ItemColors.Foreground;
                var background = index == Descriptor.SelectedItem ? Descriptor.ItemColors.Foreground : Descriptor.ItemColors.Background;

                HandleMessage(new RenderTextMessage(Descriptor.Items[index].Text, Descriptor.X, y, foreground, background));
                y += Descriptor.LineSpacing;
            }
            return true;
        }
    }
}
