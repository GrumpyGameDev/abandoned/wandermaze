﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Splorr;

namespace SplorrClient
{
    public enum MainMenuCommand
    {
        New,
        Resume,
        Open,
        Save,
        Abandon,
        Help,
        Options,
        About,
        Quit
    }
    public enum UICommand
    {
        MoveUp,
        MoveDown,
        MoveLeft,
        MoveRight,
        LookUp,
        LookDown,
        LookLeft,
        LookRight,
        MenuUp,
        MenuDown,
        MenuLeft,
        MenuRight,
        TabLeft,
        TabRight,
        FireLeft,
        FireRight,
        ActionLeft,
        ActionRight,
        Confirm,
        Cancel,
        Interact,
        Switch,
        Back,
        Start
    }
    public class GameController : MessageHandler
    {
        private Microsoft.Xna.Framework.Game _game;

        #region Game Data

        private Splorr.GameData _gameData;
        private bool HasGameData
        {
            get

            {
                return _gameData != null;
            }
        }

        #endregion

        #region Image Renderer

        private ImageRenderer _imageRenderer;
        public ImageRenderer ImageRenderer
        {
            get
            {
                return _imageRenderer;
            }
        }

        #endregion

        #region Input Focus

        public MessageHandler InputFocus { get; set; }
        public bool DispatchInput(Message message)
        {
            if (InputFocus == null || InputFocus.Disabled)
            {
                return HandleNotify(message);
            }
            else
            {
                return InputFocus.OnMessage(message);
            }
        }

        #endregion

        #region UI Command Focus

        private List<MessageHandler> _uICommandFoci = new List<MessageHandler>();
        public void AddUICommandFocus(MessageHandler handler)
        {
            _uICommandFoci.Add(handler);
        }
        public void RemoveUICommandFocus(MessageHandler handler)
        {
            _uICommandFoci.Remove(handler);
        }
        public MessageHandler UICommandFocus
        {
            get
            {
                return _uICommandFoci.FirstOrDefault(x=>!x.Disabled);
            }
        }
        public bool DispachUICommand(UICommand command)
        {
            var message = new UICommandMessage(command);
            if (UICommandFocus==null)
            {
                return HandleNotify(message);
            }
            else
            {
                return UICommandFocus.OnMessage(message);
            }
        }

        #endregion

        #region Main Menu

        private Dictionary<bool, MenuController<MainMenuCommand>> _mainMenuControllers = new Dictionary<bool, MenuController<MainMenuCommand>>();

        private void ShowMainMenu()
        {
            _mainMenuControllers[HasGameData].Disabled = false;
        }
        private void HideMainMenu()
        {
            _mainMenuControllers[false].Disabled = true;
            _mainMenuControllers[true].Disabled = true;
        }

        #endregion

        #region Play Screen

        private PlayScreenController _playScreenController;

        private void ShowPlayScreen()
        {
            if (_playScreenController.Disabled)
            {
                _playScreenController.Disabled = false;
            }
        }

        private void HidePlayScreen()
        {
            if (!_playScreenController.Disabled)
            {
                _playScreenController.Disabled = true;
            }
        }

        #endregion

        #region About Screen

        private AboutScreenController _aboutScreenController;

        #endregion

        #region Help Screen

        private HelpScreenController _helpScreenController;

        #endregion

        #region Options Screen

        private OptionsScreenController _optionsScreenController;

        #endregion

        #region Notification Screen

        private NotificationScreenController _notificationScreenController;

        #endregion

        public GameController(Microsoft.Xna.Framework.Game game, ImageRenderer imageRenderer)
            : base(null, false)
        {
            _game = game;
            _imageRenderer = imageRenderer;

            InputFocus = this;

            _playScreenController = new PlayScreenController(this, true);
            _aboutScreenController = new AboutScreenController(this, true);
            _helpScreenController = new HelpScreenController(this, true);
            _optionsScreenController = new OptionsScreenController(this, true);
            _notificationScreenController = new NotificationScreenController(this, true);

            #region Main Menu Controllers

            _mainMenuControllers[false] = new MenuController<MainMenuCommand>(this, true, new MenuDescriptor<MainMenuCommand>()
            {
                X = 80 - 9 * 4,
                Y = 60 - 7 * 4,
                LineSpacing = 8,
                SelectedItem = 0,
                MenuHeader = new MenuHeaderDescriptor()
                {
                    Colors = new ColorPair("silver", "ruby"),
                    Text = "Main Menu"
                },
                Items = new MenuItemDescriptor<MainMenuCommand>[]
                {
                    new MenuItemDescriptor<MainMenuCommand>("New Game ",MainMenuCommand.New),
                    new MenuItemDescriptor<MainMenuCommand>("Load Game",MainMenuCommand.Open),
                    new MenuItemDescriptor<MainMenuCommand>("Help     ",MainMenuCommand.Help),
                    new MenuItemDescriptor<MainMenuCommand>("Options  ",MainMenuCommand.Options),
                    new MenuItemDescriptor<MainMenuCommand>("About    ",MainMenuCommand.About),
                    new MenuItemDescriptor<MainMenuCommand>("Quit     ",MainMenuCommand.Quit)
                },
                ItemColors = new ColorPair("silver", "black")
            },
            (handler, item) => handler.HandleMessage(new MainMenuCommandMessage(item.Command)));

            _mainMenuControllers[true] = new MenuController<MainMenuCommand>(this, true, new MenuDescriptor<MainMenuCommand>()
            {
                X = 80 - 9 * 4,
                Y = 60 - 8 * 4,
                LineSpacing = 8,
                SelectedItem = 0,
                MenuHeader = new MenuHeaderDescriptor()
                {
                    Colors = new ColorPair("silver", "ruby"),
                    Text = "Main Menu"
                },
                Items = new MenuItemDescriptor<MainMenuCommand>[]
                {
                    new MenuItemDescriptor<MainMenuCommand>("Resume   ",MainMenuCommand.Resume),
                    new MenuItemDescriptor<MainMenuCommand>("Save Game",MainMenuCommand.Save),
                    new MenuItemDescriptor<MainMenuCommand>("Abandon  ",MainMenuCommand.Abandon),
                    new MenuItemDescriptor<MainMenuCommand>("Help     ",MainMenuCommand.Help),
                    new MenuItemDescriptor<MainMenuCommand>("Options  ",MainMenuCommand.Options),
                    new MenuItemDescriptor<MainMenuCommand>("About    ",MainMenuCommand.About),
                    new MenuItemDescriptor<MainMenuCommand>("Quit     ",MainMenuCommand.Quit)
                },
                ItemColors = new ColorPair("silver", "black")
            },
            (handler, item) => handler.HandleMessage(new MainMenuCommandMessage(item.Command)));

            #endregion

            AddUICommandFocus(_notificationScreenController);
            AddUICommandFocus(_mainMenuControllers[true]);
            AddUICommandFocus(_mainMenuControllers[false]);
            AddUICommandFocus(_optionsScreenController);
            AddUICommandFocus(_aboutScreenController);
            AddUICommandFocus(_playScreenController);

            #region Message Handlers

            _handlers[UICommandMessage.MessageId] = message => 
                OnUICommandMessage(message as UICommandMessage);
            _handlers[GamePadButtonDownMessage.MessageId] = message =>
                OnGamePadButtonDownMessage(message as GamePadButtonDownMessage);
            _handlers[MainMenuCommandMessage.MessageId] = message =>
                OnMainMenuCommandMessage(message as MainMenuCommandMessage);
            _handlers[QuitMessage.MessageId] = message =>
                OnQuitMessage(message as QuitMessage);
            _handlers[RenderImageMessage.MessageId] = message =>
                OnRenderImageMessage(message as RenderImageMessage);
            _handlers[RenderTextMessage.MessageId] = message =>
                OnRenderTextMessage(message as RenderTextMessage);
            _handlers[RenderMapMessage.MessageId] = message =>
                OnRenderMapMessage(message as RenderMapMessage);
            _handlers[KeyDownMessage.MessageId] = message =>
                OnKeyDownMessage(message as KeyDownMessage);
            _handlers[CancelMenuMessage.MessageId] = message =>
                OnCancelMenuMessage(message as CancelMenuMessage);
            _handlers[ShowMainMenuMessage.MessageId] = message =>
                OnShowMainMenuMessage(message as ShowMainMenuMessage);
            _handlers[FetchGameDataMessage.MessageId] = message =>
                OnFetchGameDataMessage(message as FetchGameDataMessage);
            _handlers[SetScaleMessage.MessageId] = message =>
                OnSetScaleMessage(message as SetScaleMessage);
            _handlers[FetchNotificationMessage.MessageId] = message =>
                OnFetchNotificationMessage(message as FetchNotificationMessage);
            _handlers[ClearNotificationMessage.MessageId] = message =>
                OnClearNotificationMessage(message as ClearNotificationMessage);
            _handlers[UpdateMessage.MessageId] = message =>
                OnUpdateMessage(message as UpdateMessage);

            #endregion

            ShowMainMenu();

        }

        private bool OnUpdateMessage(UpdateMessage updateMessage)
        {
            if(_notificationScreenController.Disabled && !string.IsNullOrEmpty(_gameData?.CurrentNotification))
            {
                _notificationScreenController.Disabled = false;
            }
            return false;
        }

        private bool OnClearNotificationMessage(ClearNotificationMessage clearNotificationMessage)
        {
            _gameData.ClearNotification();
            return true;
        }

        private bool OnFetchNotificationMessage(FetchNotificationMessage fetchNotificationMessage)
        {
            if(fetchNotificationMessage== null)
            {
                throw new ArgumentNullException("fetchNotificationMessage");
            }
            fetchNotificationMessage.Action(_gameData.CurrentNotification);
            return true;
        }

        private bool OnSetScaleMessage(SetScaleMessage setScaleMessage)
        {
            if(setScaleMessage== null)
            {
                throw new ArgumentNullException("setScaleMessage");
            }
            _imageRenderer.SetScale(setScaleMessage.XScale, setScaleMessage.YScale, setScaleMessage.ZScale);
            return true;
        }

        private bool OnFetchGameDataMessage(FetchGameDataMessage fetchGameDataMessage)
        {
            if(fetchGameDataMessage == null)
            {
                throw new ArgumentNullException("fetchGameDataMessage");
            }
            fetchGameDataMessage.Action(_gameData);
            return true;
        }

        private bool OnShowMainMenuMessage(ShowMainMenuMessage showMainMenuMessage)
        {
            ShowMainMenu();
            return true;
        }

        #region Message Handlers

        private Dictionary<int, Func<Message, bool>> _handlers = new Dictionary<int, Func<Message, bool>>();

        private bool OnCancelMenuMessage(CancelMenuMessage cancelMenuMessage)
        {
            var target = cancelMenuMessage.Target;
            if (target == _mainMenuControllers[false] || target == _mainMenuControllers[true])
            {
                target.Disabled = true;
                return true;
            }
            return false;
        }

        public override bool OnMessage(Message message)
        {
            if(message== null)
            {
                throw new ArgumentNullException("message");
            }
            if(_handlers.ContainsKey(message.Id))
            {
                return _handlers[message.Id](message);
            }
            else
            {
                Debug.Print("Message Id: {0}", message.Id);
                return false;
            }
        }


        private bool OnUICommandMessage(UICommandMessage uICommandMessage)
        {
            switch(uICommandMessage.Command)
            {
                case UICommand.Start:
                    ShowMainMenu();
                    return true;
                default:
                    return false;
            }
        }

        private bool OnGamePadButtonDownMessage(GamePadButtonDownMessage gamePadButtonDownMessage)
        {
            if(gamePadButtonDownMessage== null)
            {
                throw new ArgumentNullException("gamePadButtonDownMessage");
            }
            switch(gamePadButtonDownMessage.Button)
            {
                case Microsoft.Xna.Framework.Input.Buttons.A:
                    return DispachUICommand(UICommand.Confirm);
                case Microsoft.Xna.Framework.Input.Buttons.B:
                    return DispachUICommand(UICommand.Cancel);
                case Microsoft.Xna.Framework.Input.Buttons.X:
                    return DispachUICommand(UICommand.Interact);
                case Microsoft.Xna.Framework.Input.Buttons.Y:
                    return DispachUICommand(UICommand.Switch);

                case Microsoft.Xna.Framework.Input.Buttons.DPadDown:
                    return DispachUICommand(UICommand.MenuDown);
                case Microsoft.Xna.Framework.Input.Buttons.DPadUp:
                    return DispachUICommand(UICommand.MenuUp);
                case Microsoft.Xna.Framework.Input.Buttons.DPadLeft:
                    return DispachUICommand(UICommand.MenuLeft);
                case Microsoft.Xna.Framework.Input.Buttons.DPadRight:
                    return DispachUICommand(UICommand.MenuRight);

                case Microsoft.Xna.Framework.Input.Buttons.LeftThumbstickDown:
                    return DispachUICommand(UICommand.MoveDown);
                case Microsoft.Xna.Framework.Input.Buttons.LeftThumbstickUp:
                    return DispachUICommand(UICommand.MoveUp);
                case Microsoft.Xna.Framework.Input.Buttons.LeftThumbstickLeft:
                    return DispachUICommand(UICommand.MoveLeft);
                case Microsoft.Xna.Framework.Input.Buttons.LeftThumbstickRight:
                    return DispachUICommand(UICommand.MoveRight);

                case Microsoft.Xna.Framework.Input.Buttons.RightThumbstickDown:
                    return DispachUICommand(UICommand.LookDown);
                case Microsoft.Xna.Framework.Input.Buttons.RightThumbstickUp:
                    return DispachUICommand(UICommand.LookUp);
                case Microsoft.Xna.Framework.Input.Buttons.RightThumbstickLeft:
                    return DispachUICommand(UICommand.LookLeft);
                case Microsoft.Xna.Framework.Input.Buttons.RightThumbstickRight:
                    return DispachUICommand(UICommand.LookRight);

                case Microsoft.Xna.Framework.Input.Buttons.Back:
                    return DispachUICommand(UICommand.Back);
                case Microsoft.Xna.Framework.Input.Buttons.Start:
                    return DispachUICommand(UICommand.Start);

                default:
                    return false;
            }
        }

        private bool OnMainMenuCommandMessage(MainMenuCommandMessage mainMenuCommandMessage)
        {
            if(mainMenuCommandMessage== null)
            {
                throw new ArgumentNullException("mainMenuCommandMessage");
            }
            switch(mainMenuCommandMessage.Command)
            {
                case MainMenuCommand.Quit:
                    return HandleMessage(new QuitMessage());
                case MainMenuCommand.New:
                    _gameData = NewGame();
                    HideMainMenu();
                    ShowPlayScreen();
                    return true;
                case MainMenuCommand.Abandon:
                    _gameData = null;
                    HideMainMenu();
                    return true;
                default:
                    HideMainMenu();
                    return true;
            }
        }

        private Splorr.GameData NewGame()
        {
            return GameDataFactory.CreateGameData();
        }

        private bool OnQuitMessage(QuitMessage quitMessage)
        {
            _game.Exit();
            return true;
        }

        private bool OnKeyDownMessage(KeyDownMessage keyDownMessage)
        {
            if(keyDownMessage== null)
            {
                throw new ArgumentNullException("keyDownMessage");
            }
            switch(keyDownMessage.Key)
            {
                case Microsoft.Xna.Framework.Input.Keys.W:
                    return DispachUICommand(UICommand.MoveUp);
                case Microsoft.Xna.Framework.Input.Keys.A:
                    return DispachUICommand(UICommand.MoveLeft);
                case Microsoft.Xna.Framework.Input.Keys.S:
                    return DispachUICommand(UICommand.MoveDown);
                case Microsoft.Xna.Framework.Input.Keys.D:
                    return DispachUICommand(UICommand.MoveRight);

                case Microsoft.Xna.Framework.Input.Keys.Up:
                    return DispachUICommand(UICommand.MenuUp);
                case Microsoft.Xna.Framework.Input.Keys.Down:
                    return DispachUICommand(UICommand.MenuDown);
                case Microsoft.Xna.Framework.Input.Keys.Left:
                    return DispachUICommand(UICommand.MenuLeft);
                case Microsoft.Xna.Framework.Input.Keys.Right:
                    return DispachUICommand(UICommand.MenuRight);

                case Microsoft.Xna.Framework.Input.Keys.Escape:
                    return DispachUICommand(UICommand.Start);
                case Microsoft.Xna.Framework.Input.Keys.Tab:
                    return DispachUICommand(UICommand.Back);

                case Microsoft.Xna.Framework.Input.Keys.Space:
                    return DispachUICommand(UICommand.Confirm);
                case Microsoft.Xna.Framework.Input.Keys.E:
                    return DispachUICommand(UICommand.Interact);
                case Microsoft.Xna.Framework.Input.Keys.LeftShift:
                    return DispachUICommand(UICommand.Cancel);
                case Microsoft.Xna.Framework.Input.Keys.X:
                    return DispachUICommand(UICommand.Switch);

                case Microsoft.Xna.Framework.Input.Keys.NumPad8:
                    return DispachUICommand(UICommand.LookUp);
                case Microsoft.Xna.Framework.Input.Keys.NumPad2:
                    return DispachUICommand(UICommand.LookDown);
                case Microsoft.Xna.Framework.Input.Keys.NumPad4:
                    return DispachUICommand(UICommand.LookLeft);
                case Microsoft.Xna.Framework.Input.Keys.NumPad6:
                    return DispachUICommand(UICommand.LookRight);

                default:
                    return false;
            }
        }

        private bool OnRenderTextMessage(RenderTextMessage renderTextMessage)
        {
            if(renderTextMessage== null)
            {
                throw new ArgumentNullException("renderTextMessage");
            }
            _imageRenderer.RenderText(renderTextMessage.Payload.Text, renderTextMessage.Payload.X, renderTextMessage.Payload.Y, renderTextMessage.Payload.Foreground, renderTextMessage.Payload.Background);
            return true;
        }

        private bool OnRenderImageMessage(RenderImageMessage renderImageMessage)
        {
            if (renderImageMessage == null)
            {
                throw new ArgumentNullException("renderImageMessage");
            }
            _imageRenderer.Render(renderImageMessage.Payload.ImageName, renderImageMessage.Payload.X, renderImageMessage.Payload.Y);
            return true;
        }

        private bool OnRenderMapMessage(RenderMapMessage renderMapMessage)
        {
            if (renderMapMessage == null)
            {
                throw new ArgumentNullException("renderImageMessage");
            }
            _imageRenderer.RenderMap(
                renderMapMessage.Payload.Map, 
                renderMapMessage.Payload.X,
                renderMapMessage.Payload.Y,
                renderMapMessage.Payload.Columns,
                renderMapMessage.Payload.Rows);
            return true;
        }

        #endregion
    }
}
