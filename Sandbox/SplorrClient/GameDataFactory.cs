﻿using Splorr;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    internal static class GameDataFactory
    {
        internal static class Terrains
        {
            internal const string Tree = "tree";
            internal const string Grass = "grass";
        }

        internal static class Creatures
        {
            internal const string Tagon = "tagon";
        }

        internal static class Items
        {
            internal const string Key = "key";
        }

        internal static class Directions
        {
            internal const string North = "north";
            internal const string South = "south";
            internal const string East = "east";
            internal const string West = "west";

            internal static Location GetDelta(string direction)
            {
                switch(direction)
                {
                    case North:
                        return new Location(0, 0, 0, 0, 0, -1);
                    case South:
                        return new Location(0, 0, 0, 0, 0, 1);
                    case East:
                        return new Location(0, 0, 0, 0, 1, 0);
                    case West:
                        return new Location(0, 0, 0, 0, -1, 0);
                    default:
                        return new Location(0, 0, 0, 0, 0, 0);
                }
            }

            public static readonly string[] Items = new string[] { North, East, South, West };
        }



        internal static class MapConstants
        {
            internal const int Columns = 15;
            internal const int Rows =15;
            internal const int MinimumX = 0;
            internal const int MaximumX = Columns - 1;
            internal const int MinimumY = 0;
            internal const int MaximumY = Columns - 1;
            internal const int CenterX = MaximumX / 2;
            internal const int CenterY = MaximumY / 2;
        }

        internal static Splorr.GameData CreateGameData()
        {
            int atlas = 0;
            int atlasColumn = 0;
            int atlasRow = 0;
            int mapLayer = 0;

            int playerX = MapConstants.MinimumX + 2;
            int playerY = MapConstants.MinimumY + 2;

            int keyX = MapConstants.MaximumX - 2;
            int keyY = MapConstants.MinimumY + 2;

            Splorr.GameData game = new Splorr.GameData();
            game.ScriptTableType = typeof(GameScripts);


            game.World = new World();
            game.Player = new Player();
            game.Player.Location = new Location(atlas, atlasColumn, atlasRow, mapLayer, playerX, playerY);
            game.Configuration = new Configuration();

            game.Configuration.SetDialog(GameScripts.Notifications.TagonSpawned,
                new Dialog(
                    new DialogCommands(
                        new DialogCommand(null, "confirm", "dialog-close"),
                        new DialogCommand(null, "cancel", "dialog-close")),
                    new DialogSection(null, "silver", "ruby", 
                        DialogSection.MakeLines(
                            new DialogLine(null,"Welcome to Splorr!!")), 
                        DialogSection.MakeIcons()),
                    new DialogSection(null, "black", "darksilver", 
                        DialogSection.MakeLines(
                            new DialogLine(null, "Controls:"),
                            new DialogLine(null, "LS = WASD"),
                            new DialogLine(null, "RS = NumPad"),
                            new DialogLine(null, "DPad = Arrows"),
                            new DialogLine(null, "A = Space"),
                            new DialogLine(null, "B = LShift"),
                            new DialogLine(null, "X = E"),
                            new DialogLine(null, "Y = X"),
                            new DialogLine(null, "Back = Tab"),
                            new DialogLine(null, "Start = Esc")), 
                        DialogSection.MakeIcons()),
                    new DialogSection(null, "black", "jade", 
                        DialogSection.MakeLines(
                            new DialogLine(null, "[A] - Close")), 
                        DialogSection.MakeIcons())
                    ));
            game.Configuration.SetDialog(GameScripts.Notifications.TagonBumped,
                new Dialog(
                    new DialogCommands(
                        new DialogCommand(null, "confirm", "dialog-close"),
                        new DialogCommand(null, "cancel", "dialog-close")),
                    new DialogSection(null, "silver", "ruby",
                        DialogSection.MakeLines(
                            new DialogLine(null, "Blocked!")),
                        DialogSection.MakeIcons()),
                    new DialogSection(null, "black", "darksilver",
                        DialogSection.MakeLines(
                            new DialogLine(null, "Some terrain"),
                            new DialogLine(null, "is impassible.")),
                        DialogSection.MakeIcons()),
                    new DialogSection(null, "black", "jade",
                        DialogSection.MakeLines(
                            new DialogLine(null, "[A] - Close")),
                        DialogSection.MakeIcons())
                    ));
            game.Configuration.SetDialog(GameScripts.Notifications.KeyBumped,
                new Dialog(
                    new DialogCommands(
                        new DialogCommand(null, "confirm", "dialog-close"),
                        new DialogCommand(null, "cancel", "dialog-close")),
                    new DialogSection(null, "silver", "ruby",
                        DialogSection.MakeLines(
                            new DialogLine(null, "Useful Items!")),
                        DialogSection.MakeIcons()),
                    new DialogSection(null, "black", "darksilver",
                        DialogSection.MakeLines(
                            new DialogLine(null, "Pick up by moving"),
                            new DialogLine(null, "onto them.")),
                        DialogSection.MakeIcons()),
                    new DialogSection(null, "black", "jade",
                        DialogSection.MakeLines(
                            new DialogLine(null, "[A] - Close")),
                        DialogSection.MakeIcons())
                    ));
            game.Configuration.SetDialog(GameScripts.Notifications.KeyPickup,
                new Dialog(
                    new DialogCommands(
                        new DialogCommand(null, "confirm", "dialog-close"),
                        new DialogCommand(null, "cancel", "dialog-close")),
                    new DialogSection(null, "silver", "ruby",
                        DialogSection.MakeLines(
                            new DialogLine(null, "A Red Key!")),
                        DialogSection.MakeIcons()),
                    new DialogSection(null, "black", "darksilver",
                        DialogSection.MakeLines(
                            new DialogLine(null, "Usually this means"),
                            new DialogLine(null, "there is a red door.")),
                        DialogSection.MakeIcons()),
                    new DialogSection(null, "black", "jade",
                        DialogSection.MakeLines(
                            new DialogLine(null, "[A] - Close")),
                        DialogSection.MakeIcons())
                    ));

            var treeTerrain = new Terrain();
            treeTerrain.SetNewScript(
                GameScripts.Scripts.CreateTree);
            treeTerrain.SetScript(Splorr.Scripts.CanEnter, GameScripts.Scripts.CanEnterTree);
            treeTerrain.SetScript(Splorr.Scripts.OnEnter, GameScripts.Scripts.Deny);

            var grassTerrain = new Terrain();
            grassTerrain.SetNewScript(GameScripts.Scripts.CreateGrass);
            grassTerrain.SetScript(Splorr.Scripts.OnEnter, GameScripts.Scripts.Allow);

            game.Configuration.SetTerrain(Terrains.Tree, treeTerrain);
            game.Configuration.SetTerrain(Terrains.Grass, grassTerrain);

            var tagonCreature = new Creature();
            tagonCreature.SetNewScript(GameScripts.Scripts.CreateTagon);
            tagonCreature.SetOnSpawnScript(GameScripts.Scripts.SpawnTagon);
            tagonCreature.SetScript(GameScripts.Scripts.BumpTagon, GameScripts.Scripts.BumpTagon);

            game.Configuration.SetCreature(Creatures.Tagon, tagonCreature);

            var keyItem = new Item();
            keyItem.SetNewScript(GameScripts.Scripts.CreateKey);

            game.Configuration.SetItem(Items.Key, keyItem);

            var map = game.World.GetMap(atlas, atlasColumn, atlasRow, mapLayer, true);

            for (int x = MapConstants.MinimumX; x <= MapConstants.MaximumX; ++x)
            {
                for (int y = MapConstants.MinimumY; y <= MapConstants.MaximumY; ++y)
                {
                    var location = new Location(atlas, atlasColumn, atlasRow, mapLayer, x, y);
                    var mapCell = map.GetMapCell(x, y, true);

                    foreach(var direction in Directions.Items)
                    {
                        mapCell.SetNeighbor(direction, location + Directions.GetDelta(direction));
                    }
                    if (x == MapConstants.MinimumX || y == MapConstants.MinimumY || x == MapConstants.MaximumX || y == MapConstants.MaximumY)
                    {
                        game.Configuration.CreateTerrainInstance(mapCell, Terrains.Tree);
                    }
                    else
                    {
                        game.Configuration.CreateTerrainInstance(mapCell, Terrains.Grass);
                    }
                }
            }

            var keyLocation = new Location(atlas, atlasColumn, atlasRow, mapLayer, keyX, keyY);
            var key = game.Configuration.CreateItemInstance(game.World[keyLocation], Items.Key);

            var tagon = game.Configuration.CreateCreatureInstance(game.World[game.Player.Location], Creatures.Tagon);

            GameData.Save(game, "tutorial.sav");

            game = GameData.Load("tutorial.sav");

            return game;
        }
    }
}
