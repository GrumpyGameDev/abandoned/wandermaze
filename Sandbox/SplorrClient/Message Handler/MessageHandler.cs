﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public abstract class MessageHandler
    {
        private MessageHandler() { }
        public MessageHandler(MessageHandler parent, bool disabled)
        {
            Parent = parent;
            _disabled = disabled;
        }
        private bool _disabled;
        public bool Disabled
        {
            get
            {
                return _disabled;
            }
            set
            {
                if (_disabled != value)
                {
                    _disabled = value;
                }
            }
        }
        public bool Enabled
        {
            get
            {
                return !Disabled;
            }
            set
            {
                Disabled = !value;
            }
        }
        private MessageHandler _parent;
        public MessageHandler Parent
        {
            get
            {
                return _parent;
            }
            set
            {
                if(_parent!= null)
                {
                    _parent.RemoveChild(this);
                }
                _parent = value;
                if (_parent != null)
                {
                    _parent.AddChild(this);
                }
            }
        }
        public bool HasParent
        {
            get
            {
                return _parent != null;
            }
        }
        private List<MessageHandler> _children = new List<MessageHandler>();
        public IEnumerable<MessageHandler> Children
        {
            get
            {
                return _children.AsEnumerable();
            }
        }
        public bool HasChildren
        {
            get
            {
                return _children.Any();
            }
        }
        public void AddChild(MessageHandler child)
        {
            RemoveChild(child);
            _children.Add(child);
        }
        public void RemoveChild(MessageHandler child)
        {
            if(HasChild(child))
            {
                _children.Remove(child);
            }
        }
        public bool HasChild(MessageHandler child)
        {
            return _children.Contains(child);
        }
        public abstract bool OnMessage(Message message);
        public bool HandleMessage(Message message)
        {
            if(Disabled)
            {
                return false;
            }
            else if(OnMessage(message))
            {
                return true;
            }
            else if(HasParent)
            {
                return Parent.HandleMessage(message);
            }
            else
            {
                return false;
            }
        }
        public void Notify(Message message,bool reverse=false)
        {
            if(Disabled)
            {
                return;
            }
            if(!reverse)
            {
                OnMessage(message);
            }
            for (int index = 0; index < _children.Count(); ++index)
            {
                _children[reverse ? _children.Count()-index : index].Notify(message, reverse);
            }
            if(reverse)
            {
                OnMessage(message);
            }
        }
        public bool HandleNotify(Message message, bool reverse=true)
        {
            if (Disabled)
            {
                return false;
            }
            if (!reverse)
            {
                if(OnMessage(message))
                {
                    return true;
                }
            }
            for (int index = 0; index < _children.Count(); ++index)
            {
                if(_children[reverse ? _children.Count() - index -1 : index].HandleNotify(message, reverse))
                {
                    return true;
                }
            }
            if (reverse)
            {
                return OnMessage(message);
            }
            return false;
        }
    }
}
