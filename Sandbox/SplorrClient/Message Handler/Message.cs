﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public abstract class Message
    {
        private Message()
        { }

        private int _id;
        public Message(int id)
        {
            _id = id;
        }
        public int Id
        {
            get
            {
                return _id;
            }
        }
    }
}
