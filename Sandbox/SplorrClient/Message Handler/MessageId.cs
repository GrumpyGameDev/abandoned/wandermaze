﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SplorrClient
{
    public static class MessageId
    {
        private static int _nextMessageId = 0;

        public static int NextMessageId
        {
            get
            {
                int result = _nextMessageId;
                _nextMessageId++;
                return result;
            }
        }

    }
}
