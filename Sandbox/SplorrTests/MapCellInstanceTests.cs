﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Splorr;

namespace SplorrTests
{
    [TestClass]
    public class MapCellInstanceTests
    {
        [TestMethod]
        public void CreateTerrainInstance()
        {
            const string terrainName = "empty";
            const string scriptName = "new";
            const string propertyName = "testProperty";
            const string propertyValue = "testValue";
            const string isPassable = "isPassable";
            const string returnTrue = "return true";
            const string scriptFormat = "instance.setStringProperty(\"{0}\",\"{1}\")\r\ninstance.setScript(\"{2}\",\"{3}\")";

            GameData game = new GameData();
            game.Configuration = new Configuration();
            game.Configuration.SetTerrain(terrainName, new Terrain());
            game.Configuration.GetTerrain(terrainName).SetScript(scriptName, string.Format(scriptFormat,propertyName,propertyValue,isPassable,returnTrue));
            TerrainInstance instance = game.Configuration.CreateTerrainInstance(null, terrainName);

            Assert.IsNotNull(instance);
            Assert.IsNull(instance.MapCell);
            Assert.AreEqual(instance.GetStringProperty(propertyName), propertyValue);
            Assert.AreEqual(instance.GetScript(isPassable), returnTrue);
        }
    }
}
