﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Splorr;

namespace SplorrTests
{
    [TestClass]
    public class GameSerializationTests
    {
        private const string SaveFileName= "game.save";

        [TestMethod]
        public void SerializeGameEmpty()
        {
            GameData game = new GameData();

            GameData.Save(game, SaveFileName);

            game = GameData.Load(SaveFileName);

            Assert.IsNotNull(game);
            Assert.IsNull(game.Player);
            Assert.IsNull(game.World);
            Assert.IsNull(game.Configuration);
        }

        [TestMethod]
        public void SerializeGameWithEmptyPlayer()
        {
            GameData game = new GameData();
            game.Player = new Player();

            Assert.AreSame(game, game.Player.GameData);

            GameData.Save(game, SaveFileName);

            game = GameData.Load(SaveFileName);

            Assert.IsNotNull(game);
            Assert.IsNotNull(game.Player);
            Assert.IsNull(game.World);
            Assert.IsNull(game.Configuration);
            Assert.AreSame(game, game.Player.GameData);
        }

        [TestMethod]
        public void SerializeGameWithEmptyWorld()
        {
            GameData game = new GameData();
            game.World = new World();

            Assert.AreSame(game, game.World.Game);

            GameData.Save(game, SaveFileName);

            game = GameData.Load(SaveFileName);

            Assert.IsNotNull(game);
            Assert.IsNotNull(game.World);
            Assert.IsNull(game.Player);
            Assert.IsNull(game.Configuration);
            Assert.AreSame(game, game.World.Game);
        }

        [TestMethod]
        public void SerializeGameWithEmptyConfiguration()
        {
            GameData game = new GameData();
            game.Configuration = new Configuration();

            Assert.AreSame(game, game.Configuration.Game);

            GameData.Save(game, SaveFileName);

            game = GameData.Load(SaveFileName);

            Assert.IsNotNull(game);
            Assert.IsNotNull(game.Configuration);
            Assert.IsNull(game.Player);
            Assert.IsNull(game.World);
            Assert.AreSame(game, game.Configuration.Game);
        }
    }
}
