﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Sandbox
{
    class Program
    {
        static void Main(string[] args)
        {
            Splorr.GameData game = new Splorr.GameData();
            game.Player = new Splorr.Player();
            game.World = new Splorr.World();
            game.World[0] = new Splorr.Atlas();
            game.ConnectChildren();

        }
    }
}
