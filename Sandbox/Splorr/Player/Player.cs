﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class Player: InstanceBase, IHierarchical
    {
        public GameData GameData { get; set; }
        private Location _location;
        public Location Location
        {
            get
            {
                return _location;
            }
            set
            {
                _location = value;
            }
        }

        public void ConnectChildren()
        {
        }

        public void Move(string direction)
        {
            var mapCell = GameData.World[Location];

            var creatureInstance = mapCell.CreatureInstance;

            var neighbor = mapCell.GetNeighbor(direction);

            if (neighbor.HasValue)
            {
                var nextMapCell = GameData.World[neighbor.Value];

                if (nextMapCell.CanEnter(creatureInstance))
                {

                    if(nextMapCell.PeekItem()!= null)
                    {
                        nextMapCell.PeekItem().Pickup(creatureInstance);

                        nextMapCell.PopItem();
                    }
                    else
                    {
                        mapCell.CreatureInstance = null;

                        Location = neighbor.Value;

                        nextMapCell.CreatureInstance = creatureInstance;

                        nextMapCell.OnEnter(creatureInstance);
                    }
                }
            }
        }

        public override object RunScript(string scriptName, InstanceBase self, Descriptor selfDescriptor, InstanceBase other, Descriptor otherDescriptor, MapCell where)
        {
            return GameData?.RunScript(GetScript(scriptName), self, selfDescriptor, other, otherDescriptor, where);
        }
    }
}
