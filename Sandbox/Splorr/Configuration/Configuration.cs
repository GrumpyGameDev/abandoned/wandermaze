﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class Configuration:IHierarchical
    {
        public static class Script
        {
            public const string Instance = "instance";
            public const string CreatureInstance = "creatureInstance";
            public const string Descriptor = "descriptor";
            public const string MapCell = "mapCell";
        }


        [NonSerialized]
        public GameData Game;
        private Dictionary<string, Terrain> _terrains = new Dictionary<string, Terrain>();
        private Dictionary<string, Item> _items = new Dictionary<string, Item>();
        private Dictionary<string, Creature> _creatures = new Dictionary<string, Creature>();
        private Dictionary<string, Effect> _effects = new Dictionary<string, Effect>();
        private Dictionary<string, Dialog> _dialogs = new Dictionary<string, Dialog>();

        public Terrain GetTerrain(string index)
        {
            return _terrains == null ? null : _terrains[index];
        }

        public Item GetItem(string index)
        {
            return _items == null ? null : _items[index];
        }

        public Creature GetCreature(string index)
        {
            return _creatures == null ? null : _creatures[index];
        }

        public Effect GetEffect(string index)
        {
            return _effects == null ? null : _effects[index];
        }

        public Dialog GetDialog(string index)
        {
            return _dialogs == null ? null : _dialogs[index];
        }

        private static Dictionary<string, TDescriptor> SetDescriptor<TDescriptor>(Configuration configuration, Dictionary<string, TDescriptor> table, string index, TDescriptor descriptor) where TDescriptor:IDescriptor
        {
            if (table == null)
            {
                table = new Dictionary<string, TDescriptor>();
            }
            if (table.ContainsKey(index))
            {
                if (table[index] != null)
                {
                    table[index].Configuration = null;
                }
                table.Remove(index);
            }
            if (descriptor != null)
            {
                table[index] = descriptor;
                table[index].Configuration = configuration;
            }
            return table;
        }

        public void SetTerrain(string index, Terrain value)
        {
            _terrains = SetDescriptor(this, _terrains, index, value);
        }

        public void SetItem(string index, Item value)
        {
            _items = SetDescriptor(this, _items, index, value);
        }

        public void SetCreature(string index, Creature value)
        {
            _creatures = SetDescriptor(this, _creatures, index, value);
        }

        public void SetEffect(string index, Effect value)
        {
            _effects = SetDescriptor(this, _effects, index, value);
        }

        public void SetDialog(string index, Dialog value)
        {
            _dialogs = _dialogs.SetSanitized(index, value);
        }

        private static TInstance CreateInstance<TInstance,TDescriptor>(MapCell mapCell, Action<MapCell,TInstance> placementAction, Action<TInstance,string> nameAction, Func<string,TDescriptor> descriptorFetcher, string index) where TInstance: InstanceBase, IMapCellInstance, new() where TDescriptor:Descriptor
        {
            TInstance instance = new TInstance();
            nameAction(instance, index);
            placementAction(mapCell, instance);

            var descriptor = descriptorFetcher(index);
            if (descriptor != null)
            {
                descriptor.RunScript(Splorr.Scripts.New, instance, descriptor, null, null, mapCell);
            }

            return instance;
        }

        public TerrainInstance CreateTerrainInstance(MapCell mapCell, string index)
        {
            return CreateInstance<TerrainInstance, Terrain>(mapCell,(where,what)=>where.TerrainInstance=what, (instance, terrain) => instance.DescriptorIdentifier = terrain, GetTerrain, index);
        }

        public ItemInstance CreateItemInstance(MapCell mapCell, string index)
        {
            return CreateInstance<ItemInstance, Item>(mapCell, (where, what) => where.PushItem(what), (instance, item) => instance.DescriptorIdentifier = item, GetItem, index);
        }

        public CreatureInstance CreateCreatureInstance(MapCell mapCell, string index)
        {
            var creatureInstance = CreateInstance<CreatureInstance, Creature>(mapCell, (where, what) => where.CreatureInstance = what, (instance, creature) => instance.DescriptorIdentifier = creature, GetCreature, index);
            creatureInstance.Spawn();
            return creatureInstance;
        }

        public EffectInstance CreateEffectInstance(MapCell mapCell, string index)
        {
            return CreateInstance<EffectInstance, Effect>(mapCell, (where, what) => where.EffectInstance = what, (instance, effect) => instance.DescriptorIdentifier = effect, GetEffect, index);
        }

        private static void ConfigurationConnect<TDescriptor>(Configuration configuration, Dictionary<string,TDescriptor> table) where TDescriptor:IDescriptor
        {
            if (table != null)
            {
                table.Values.ToList().ForEach(x => { x.Configuration = configuration; x.ConnectChildren(); });
            }
        }

        public void ConnectChildren()
        {
            ConfigurationConnect(this, _terrains);
            ConfigurationConnect(this, _items);
            ConfigurationConnect(this, _creatures);
            ConfigurationConnect(this, _effects);
            if (_dialogs != null)
            {
                _dialogs.Values.ToList().ForEach(x =>x.Configuration = this);
            }
        }
    }
}
