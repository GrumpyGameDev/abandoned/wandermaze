﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class Dialog
    {
        private Configuration _configuration;
        public Configuration Configuration
        {
            get
            {
                return _configuration;
            }
            set
            {
                _configuration = value;
            }
        }

        private DialogCommands _commands;
        public DialogCommands Commands
        {
            get
            {
                return _commands;
            }
        }
        private DialogSection[] _sections;
        public DialogSection[] Sections
        {
            get
            {
                return _sections;
            }
        }
        public Dialog(DialogCommands commands, params DialogSection[] sections)
        {
            _sections = sections;
        }
    }
}
