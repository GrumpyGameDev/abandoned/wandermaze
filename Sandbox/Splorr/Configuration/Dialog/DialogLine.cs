﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class DialogLine : ConditionalDialogElement
    {
        private string _text;
        public string Text
        {
            get
            {
                return _text;
            }
        }
        public DialogLine(string condition, string text)
            :base(condition)
        {
            _text = text;
        }
    }
}
