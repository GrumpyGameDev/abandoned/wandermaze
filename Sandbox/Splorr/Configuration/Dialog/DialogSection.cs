﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class DialogSection: ConditionalDialogElement
    {
        private DialogLine[] _lines;
        public DialogLine[] Lines
        {
            get
            {
                return _lines;
            }
        }
        private DialogIcon[] _icons;
        public DialogIcon[] Icons
        {
            get
            {
                return _icons;
            }
        }
        private string _foreground;
        public string Foreground
        {
            get
            {
                return _foreground;
            }
        }
        private string _background;
        public string Background
        {
            get
            {
                return _background;
            }
        }
        public DialogSection(string condition, string foreground, string background, DialogLine[] lines, DialogIcon[] icons)
            :base(condition)
        {
            _foreground = foreground;
            _background = background;
            _lines = lines;
            _icons = icons;
        }
        public static DialogLine[] MakeLines(params DialogLine[] _lines)
        {
            return _lines;
        }
        public static DialogIcon[] MakeIcons(params DialogIcon[] _icons)
        {
            return _icons;
        }
    }
}
