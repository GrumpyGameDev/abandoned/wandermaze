﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class DialogCommands
    {
        private DialogCommand[] _commands;
        public DialogCommand[] Commands
        {
            get
            {
                return _commands;
            }
        }
        public DialogCommands(params DialogCommand[] commands)
        {
            _commands = commands;
        }
    }
}
