﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class DialogIcon: ConditionalDialogElement
    {
        private int _x;
        public int X
        {
            get
            {
                return _x;
            }
        }
        private int _y;
        public int Y
        {
            get
            {
                return _y;
            }
        }
        private string _image;
        public string Image
        {
            get
            {
                return _image;
            }
        }
        public DialogIcon(string condition, int x, int y, string image)
            :base(condition)
        {
            _x = x;
            _y = y;
            _image = image;
        }
    }
}
