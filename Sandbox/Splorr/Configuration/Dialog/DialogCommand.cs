﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class DialogCommand: ConditionalDialogElement
    {
        private string _from;
        public string From
        {
            get
            {
                return _from;
            }
        }
        private string _to;
        public string To
        {
            get
            {
                return _to;
            }
        }
        public DialogCommand(string condition,string from, string to)
            :base(condition)
        {
            _from = from;
            _to = to;
        }
    }
}
