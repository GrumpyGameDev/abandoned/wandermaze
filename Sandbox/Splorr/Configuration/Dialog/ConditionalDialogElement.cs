﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public abstract class ConditionalDialogElement
    {
        private string _condition;
        public string Condition
        {
            get
            {
                return _condition;
            }
        }
        public ConditionalDialogElement(string condition)
        {
            _condition = condition;
        }
    }
}
