﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class Creature : Descriptor
    {
        public override void ConnectChildren()
        {
        }

        public override object RunScript(string scriptName, InstanceBase self, Descriptor selfDescriptor, InstanceBase other, Descriptor otherDescriptor, MapCell where)
        {
            return Configuration?.Game?.RunScript(GetScript(scriptName), self, selfDescriptor, other, otherDescriptor, where);
        }
    }
}
