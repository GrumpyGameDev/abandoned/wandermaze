﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public struct Location
    {
        public int Atlas;
        public int AtlasColumn;
        public int AtlasRow;
        public int Map;
        public int MapColumn;
        public int MapRow;

        public Location(int atlas,int atlasColumn,int atlasRow,int map,int mapColumn,int mapRow)
        {
            Atlas = atlas;
            AtlasColumn = atlasColumn;
            AtlasRow = atlasRow;
            Map = map;
            MapColumn = mapColumn;
            MapRow = mapRow;

        }
        public static Location operator +(Location first, Location second) =>
            new Location(first.Atlas + second.Atlas,
                first.AtlasColumn + second.AtlasColumn,
                first.AtlasRow + second.AtlasRow,
                first.Map + second.Map,
                first.MapColumn + second.MapColumn,
                first.MapRow + second.MapRow);

    }
}
