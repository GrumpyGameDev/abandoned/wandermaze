﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class Atlas: IHierarchical
    {
        [NonSerialized]
        public World World;
        private Dictionary<int, AtlasColumn> _atlasColumns = new Dictionary<int, AtlasColumn>();
        public GameData Game
        {
            get
            {
                return World == null ? null : World.Game;
            }

        }
        public AtlasColumn this[int index]
        {
            get
            {
                return _atlasColumns.GetSanitized(index);
            }
            set
            {
                _atlasColumns = _atlasColumns.SetSanitized(index, value);
                _atlasColumns[index].Atlas = this;
            }
        }

        public void SetAtlasColumn(int atlasColumn, AtlasColumn value)
        {
            this[atlasColumn] = value;
        }

        public AtlasColumn GetAtlasColumn(int atlasColumn, bool autoCreate = false)
        {
            var result = this == null ? null : this[atlasColumn];
            if (result == null && autoCreate)
            {
                result = new AtlasColumn();
                SetAtlasColumn(atlasColumn, result);
            }
            return result;
        }

        public void SetAtlasCell(int atlasColumn, int atlasRow, AtlasCell value)
        {
            if (GetAtlasColumn(atlasColumn) == null)
            {
                SetAtlasColumn(atlasColumn, new AtlasColumn());
            }
            this[atlasColumn][atlasRow] = value;
        }

        public AtlasCell GetAtlasCell(int atlasColumn, int atlasRow, bool autoCreate = false)
        {
            var result = GetAtlasColumn(atlasColumn) == null ? null : this[atlasColumn][atlasRow];
            if (result == null && autoCreate)
            {
                result = new AtlasCell();
                SetAtlasCell(atlasColumn, atlasRow, result);
            }
            return result;
        }

        public void SetMap(int atlasColumn, int atlasRow, int map, Map value)
        {
            if (GetAtlasCell(atlasColumn, atlasRow) == null)
            {
                SetAtlasCell(atlasColumn, atlasRow, new AtlasCell());
            }
            this[atlasColumn][atlasRow][map] = value;
        }

        public Map GetMap(int atlasColumn, int atlasRow, int map, bool autoCreate = false)
        {
            var result = GetAtlasCell(atlasColumn, atlasRow) == null ? null : this[atlasColumn][atlasRow][map];
            if (result == null && autoCreate)
            {
                result = new Map();
                SetMap(atlasColumn, atlasRow, map, result);
            }
            return result;
        }

        public void SetMapColumn(int atlasColumn, int atlasRow, int map, int mapColumn, MapColumn value)
        {
            if (GetMap(atlasColumn, atlasRow, map) == null)
            {
                SetMap(atlasColumn, atlasRow, map, new Map());
            }
            this[atlasColumn][atlasRow][map][mapColumn] = value;
        }

        internal void Notify(string notification)
        {
            World?.Notify(notification);
        }

        public MapColumn GetMapColumn(int atlasColumn, int atlasRow, int map, int mapColumn, bool autoCreate = false)
        {
            var result = GetMap(atlasColumn, atlasRow, map) == null ? null : this[atlasColumn][atlasRow][map][mapColumn];
            if (result == null && autoCreate)
            {
                result = new MapColumn();
                SetMapColumn(atlasColumn, atlasRow, map, mapColumn, result);
            }
            return result;
        }

        public void SetMapCell(int atlasColumn, int atlasRow, int map, int mapColumn, int mapRow, MapCell value)
        {
            if (GetMapColumn(atlasColumn, atlasRow, map, mapColumn) == null)
            {
                SetMapColumn(atlasColumn, atlasRow, map, mapColumn, new MapColumn());
            }
            this[atlasColumn][atlasRow][map][mapColumn][mapRow] = value;
        }

        public MapCell GetMapCell(int atlasColumn, int atlasRow, int map, int mapColumn, int mapRow, bool autoCreate = false)
        {
            var result = GetMapColumn(atlasColumn, atlasRow, map, mapColumn) == null ? null : this[atlasColumn][atlasRow][map][mapColumn][mapRow];
            if (result == null && autoCreate)
            {
                result = new MapCell();
                SetMapCell(atlasColumn, atlasRow, map, mapColumn, mapRow, result);
            }
            return result;
        }


        public void ConnectChildren()
        {
            if(_atlasColumns!=null)
            {
                _atlasColumns.Values.ToList().ForEach(x => { x.Atlas = this; x.ConnectChildren(); });
            }
        }
    }
}
