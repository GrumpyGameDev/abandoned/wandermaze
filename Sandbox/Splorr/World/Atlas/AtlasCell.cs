﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class AtlasCell : IHierarchical
    {
        [NonSerialized]
        public AtlasColumn AtlasColumn;
        public Atlas Atlas
        {
            get
            {
                return AtlasColumn == null ? null : AtlasColumn.Atlas;
            }
        }
        public World World
        {
            get
            {
                return Atlas == null ? null : Atlas.World;
            }
        }
        public GameData Game
        {
            get
            {
                return World == null ? null : World.Game;
            }

        }
        private Dictionary<int, Map> _maps = new Dictionary<int, Map>();
        public Map this[int index]
        {
            get
            {
                return (_maps == null) ? null : _maps[index];
            }
            set
            {
                if (_maps == null)
                {
                    _maps = new Dictionary<int, Map>();
                }
                _maps[index] = value;
                _maps[index].AtlasCell = this;
            }
        }

        public void SetMap(int map, Map value)
        {
            this[map] = value;
        }

        public Map GetMap(int map, bool autoCreate = false)
        {
            var result = this[map];
            if (result == null && autoCreate)
            {
                result = new Map();
                SetMap(map, result);
            }
            return result;
        }

        public void SetMapColumn(int map, int mapColumn, MapColumn value)
        {
            if (GetMap(map) == null)
            {
                SetMap(map, new Map());
            }
            this[map][mapColumn] = value;
        }

        public MapColumn GetMapColumn(int map, int mapColumn, bool autoCreate = false)
        {
            var result = GetMap(map) == null ? null : this[map][mapColumn];
            if (result == null && autoCreate)
            {
                result = new MapColumn();
                SetMapColumn(map, mapColumn, result);
            }
            return result;
        }

        public void SetMapCell(int map, int mapColumn, int mapRow, MapCell value)
        {
            if (GetMapColumn(map, mapColumn) == null)
            {
                SetMapColumn(map, mapColumn, new MapColumn());
            }
            this[map][mapColumn][mapRow] = value;
        }

        internal void Notify(string notification)
        {
            AtlasColumn?.Notify(notification);
        }

        public MapCell GetMapCell(int map, int mapColumn, int mapRow, bool autoCreate = false)
        {
            var result = GetMapColumn(map, mapColumn) == null ? null : this[map][mapColumn][mapRow];
            if (result == null && autoCreate)
            {
                result = new MapCell();
                SetMapCell(map, mapColumn, mapRow, result);
            }
            return result;
        }

        public void ConnectChildren()
        {
            if(_maps!=null)
            {
                _maps.Values.ToList().ForEach(x => { x.AtlasCell = this; x.ConnectChildren(); });
            }
        }
    }
}
