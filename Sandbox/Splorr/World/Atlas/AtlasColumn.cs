﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class AtlasColumn : IHierarchical
    {
        [NonSerialized]
        public Atlas Atlas;
        private Dictionary<int, AtlasCell> _atlasCells = new Dictionary<int, AtlasCell>();
        public World World
        {
            get
            {
                return Atlas == null ? null : Atlas.World;
            }
        }
        public GameData Game
        {
            get
            {
                return World == null ? null : World.Game;
            }

        }
        public AtlasCell this[int index]
        {
            get
            {
                return (_atlasCells == null) ? null : _atlasCells[index];
            }
            set
            {
                if (_atlasCells == null)
                {
                    _atlasCells = new Dictionary<int, AtlasCell>();
                }
                _atlasCells[index] = value;
                _atlasCells[index].AtlasColumn = this;
            }
        }

        public void SetAtlasCell(int atlasRow, AtlasCell value)
        {
            this[atlasRow] = value;
        }

        public AtlasCell GetAtlasCell(int atlasRow, bool autoCreate = false)
        {
            var result = this[atlasRow];
            if (result == null && autoCreate)
            {
                result = new AtlasCell();
                SetAtlasCell( atlasRow, result);
            }
            return result;
        }

        public void SetMap(int atlasRow, int map, Map value)
        {
            if (GetAtlasCell( atlasRow) == null)
            {
                SetAtlasCell( atlasRow, new AtlasCell());
            }
            this[atlasRow][map] = value;
        }

        public Map GetMap(int atlasRow, int map, bool autoCreate = false)
        {
            var result = GetAtlasCell( atlasRow) == null ? null : this[atlasRow][map];
            if (result == null && autoCreate)
            {
                result = new Map();
                SetMap( atlasRow, map, result);
            }
            return result;
        }

        public void SetMapColumn(int atlasRow, int map, int mapColumn, MapColumn value)
        {
            if (GetMap( atlasRow, map) == null)
            {
                SetMap( atlasRow, map, new Map());
            }
            this[atlasRow][map][mapColumn] = value;
        }

        public MapColumn GetMapColumn(int atlasRow, int map, int mapColumn, bool autoCreate = false)
        {
            var result = GetMap( atlasRow, map) == null ? null : this[atlasRow][map][mapColumn];
            if (result == null && autoCreate)
            {
                result = new MapColumn();
                SetMapColumn( atlasRow, map, mapColumn, result);
            }
            return result;
        }

        internal void Notify(string notification)
        {
            Atlas?.Notify(notification);
        }

        public void SetMapCell(int atlasRow, int map, int mapColumn, int mapRow, MapCell value)
        {
            if (GetMapColumn( atlasRow, map, mapColumn) == null)
            {
                SetMapColumn( atlasRow, map, mapColumn, new MapColumn());
            }
            this[atlasRow][map][mapColumn][mapRow] = value;
        }

        public MapCell GetMapCell(int atlasRow, int map, int mapColumn, int mapRow, bool autoCreate = false)
        {
            var result = GetMapColumn( atlasRow, map, mapColumn) == null ? null : this[atlasRow][map][mapColumn][mapRow];
            if (result == null && autoCreate)
            {
                result = new MapCell();
                SetMapCell( atlasRow, map, mapColumn, mapRow, result);
            }
            return result;
        }

        public void ConnectChildren()
        {
            if (_atlasCells != null)
            {
                _atlasCells.Values.ToList().ForEach(x => { x.AtlasColumn = this; x.ConnectChildren(); });
            }
        }
    }
}
