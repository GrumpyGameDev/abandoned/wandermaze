﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class TerrainInstance : MapCellInstance
    {
        public override Descriptor Descriptor
        {
            get
            {
                return Game?.Configuration?.GetTerrain(DescriptorIdentifier);
            }
        }

        public override void ConnectChildren()
        {
        }


        internal bool CanEnter(CreatureInstance instance)
        {
            //bool property trumps anything else
            if(GetBoolProperty(Scripts.CanEnter)??false)
            {
                return true;
            }

            //attempt to handle with instance script
            var result = (bool)this.RunOptionalScript(Scripts.CanEnter, this, Descriptor, instance, instance.Descriptor, MapCell, false);

            if (result)
            {
                return result;
            }

            return (bool)Descriptor.RunOptionalScript(Scripts.CanEnter, this, Descriptor, instance, instance.Descriptor, MapCell,false);
        }

        internal void OnEnter(CreatureInstance instance)
        {
            if(!(bool)this.RunOptionalScript(Scripts.OnEnter, this, Descriptor, instance, instance.Descriptor, MapCell,false))
            {
                Descriptor.RunScript(Scripts.OnEnter, this, Descriptor, instance, instance.Descriptor, MapCell);
            }
        }
    }
}
