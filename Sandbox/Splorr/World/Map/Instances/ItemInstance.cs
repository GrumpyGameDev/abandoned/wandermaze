﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class ItemInstance : MapCellInstance
    {
        public override Descriptor Descriptor
        {
            get
            {
                return Game?.Configuration?.GetItem(DescriptorIdentifier);
            }
        }

        public override void ConnectChildren()
        {
        }

        internal bool CanPickup(CreatureInstance instance)
        {
            return (bool)this.RunOptionalScript(Scripts.CanPickup, this, Descriptor, instance, instance.Descriptor, this.MapCell, false);
        }

        internal void Pickup(CreatureInstance creatureInstance)
        {
            this.RunOptionalScript(Scripts.OnPickup, this, Descriptor, creatureInstance, creatureInstance.Descriptor, this.MapCell, false);
        }
    }
}
