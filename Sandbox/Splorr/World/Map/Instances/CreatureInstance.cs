﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class CreatureInstance : MapCellInstance
    {
        public override Descriptor Descriptor
        {
            get
            {
                return Game?.Configuration?.GetCreature(DescriptorIdentifier);
            }
        }

        public override void ConnectChildren()
        {
        }

        public void Spawn()
        {
            var creature = Game?.Configuration?.GetCreature(DescriptorIdentifier);
            creature?.RunScript(Scripts.OnSpawn, this, creature, null, null, null);
        }

        internal void OnEnter(MapCell mapCell)
        {
            var creature = Game?.Configuration?.GetCreature(DescriptorIdentifier);
            RunScript(Scripts.OnEnter, this, creature, null, null, mapCell);
        }

        internal bool CanEnter(MapCell mapCell)
        {
            return (bool)this.RunOptionalScript(Scripts.CanEnter, this, Descriptor, null, null, mapCell, true);
        }
    }
}
