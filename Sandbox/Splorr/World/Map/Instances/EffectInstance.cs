﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class EffectInstance : MapCellInstance
    {
        public override Descriptor Descriptor
        {
            get
            {
                return Game?.Configuration?.GetEffect(DescriptorIdentifier);
            }
        }

        public override void ConnectChildren()
        {
        }
    }
}
