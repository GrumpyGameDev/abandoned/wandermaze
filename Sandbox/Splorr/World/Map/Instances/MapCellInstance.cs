﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public abstract class MapCellInstance : InstanceBase, IMapCellInstance
    {
        public MapCell MapCell { get; set; }
        private string _descriptorIdentifier;
        public string DescriptorIdentifier
        {
            get
            {
                return _descriptorIdentifier;
            }
            set
            {
                _descriptorIdentifier = value;
            }
        }
        public abstract Descriptor Descriptor
        {
            get;
        }
        public string ImageName
        {
            get
            {
                return GetStringProperty(Properties.ImageName);
            }
        }

        public void Notify(string notification)
        {
            MapCell?.Notify(notification);
        }

        public MapColumn MapColumn
        {
            get
            {
                return MapCell == null ? null : MapCell.MapColumn;
            }
        }
        public Map Map
        {
            get
            {
                return MapColumn == null ? null : MapColumn.Map;

            }
        }
        public AtlasCell AtlasCell
        {
            get
            {
                return Map == null ? null : Map.AtlasCell;

            }
        }
        public AtlasColumn AtlasColumn
        {
            get
            {
                return AtlasCell == null ? null : AtlasCell.AtlasColumn;

            }
        }
        public Atlas Atlas
        {
            get
            {
                return AtlasColumn == null ? null : AtlasColumn.Atlas;
            }
        }
        public World World
        {
            get
            {
                return Atlas == null ? null : Atlas.World;
            }
        }
        public GameData Game
        {
            get
            {
                return World == null ? null : World.Game;
            }
        }
        public abstract void ConnectChildren();
        public override object RunScript(string scriptName, InstanceBase self, Descriptor selfDescriptor, InstanceBase other, Descriptor otherDescriptor, MapCell where)
        {
            return Game?.RunScript(GetScript(scriptName), self, selfDescriptor, other, otherDescriptor, where);
        }

    }
}
