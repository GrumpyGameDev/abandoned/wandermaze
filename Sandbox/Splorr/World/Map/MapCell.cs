﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class MapCell : IHierarchical
    {
        #region Terrain

        private TerrainInstance _terrainInstance;
        public TerrainInstance TerrainInstance
        {
            get
            {
                return _terrainInstance;
            }
            set
            {
                if (_terrainInstance != null)
                {
                    _terrainInstance.MapCell = null;
                }
                _terrainInstance = value;
                if (_terrainInstance != null)
                {
                    _terrainInstance.MapCell = this;
                }
            }
        }

        #endregion

        #region Creatures

        private CreatureInstance _creatureInstance;
        public CreatureInstance CreatureInstance
        {
            get
            {
                return _creatureInstance;
            }
            set
            {
                if (_creatureInstance != null)
                {
                    _creatureInstance.MapCell = null;
                }
                _creatureInstance = value;
                if (_creatureInstance != null)
                {
                    _creatureInstance.MapCell = this;
                }
            }
        }

        internal void OnEnter(CreatureInstance instance)
        {
            if(TerrainInstance!= null)
            {
                TerrainInstance.OnEnter(instance);
            }
            instance.OnEnter(this);
        }

        public bool CanEnter(CreatureInstance instance)
        {
            //if there is already a creature here, then no
            if(CreatureInstance!= null)
            {
                return false;
            }
            //if there are items, then no
            if(PeekItem()!= null)
            {
                if (PeekItem().CanPickup(instance))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            //if no terrain, then no
            if(TerrainInstance== null)
            {
                return false;
            }
            return TerrainInstance.CanEnter(instance) && instance.CanEnter(this);
        }

        internal void Notify(string notification)
        {
            MapColumn?.Notify(notification);
        }

        #endregion

        #region Effects

        private EffectInstance _effectInstance;
        public EffectInstance EffectInstance
        {
            get
            {
                return _effectInstance;
            }
            set
            {
                if (_effectInstance != null)
                {
                    _effectInstance.MapCell = null;
                }
                _effectInstance = value;
                if (_effectInstance != null)
                {
                    _effectInstance.MapCell = this;
                }
            }
        }

        #endregion

        #region Items

        private Stack<ItemInstance> _itemInstances = new Stack<ItemInstance>();
        public void PushItem(ItemInstance itemInstance)
        {
            if (itemInstance != null)
            {
                itemInstance.MapCell = this;
                _itemInstances.Push(itemInstance);
            }
        }
        public ItemInstance PeekItem()
        {
            return _itemInstances.Any() ? _itemInstances.Peek() : null;
        }

        public ItemInstance PopItem()
        {
            ItemInstance instance = null;
            if (_itemInstances.Any())
            {
                instance = _itemInstances.Pop();
                instance.MapCell = null;
            }
            return instance;
        }

        #endregion

        #region Neighbors

        private Dictionary<string, Location> _neighbors = new Dictionary<string, Location>();

        public Location? GetNeighbor(string index)
        {
            if(_neighbors.ContainsKey(index))
            {
                return _neighbors[index];
            }
            else
            {
                return null;
            }
        }

        public void SetNeighbor(string index, Location? location)
        {
            if(location== null)
            {
                if(_neighbors.ContainsKey(index))
                {
                    _neighbors.Remove(index);
                }
            }
            else
            {
                _neighbors[index] = location.Value;
            }
        }

        #endregion

        #region Hierarchical Navigation
        public MapColumn MapColumn { get; set; }

        public Map Map
        {
            get
            {
                return MapColumn == null ? null : MapColumn.Map;

            }
        }
        public AtlasCell AtlasCell
        {
            get
            {
                return Map == null ? null : Map.AtlasCell;

            }
        }
        public AtlasColumn AtlasColumn
        {
            get
            {
                return AtlasCell == null ? null : AtlasCell.AtlasColumn;

            }
        }
        public Atlas Atlas
        {
            get
            {
                return AtlasColumn == null ? null : AtlasColumn.Atlas;
            }
        }
        public World World
        {
            get
            {
                return Atlas == null ? null : Atlas.World;
            }
        }
        public GameData Game
        {
            get
            {
                return World == null ? null : World.Game;
            }
        }

        #endregion

        public void ConnectChildren()
        {
            TerrainInstance = TerrainInstance;
            TerrainInstance?.ConnectChildren();

            CreatureInstance = CreatureInstance;
            CreatureInstance?.ConnectChildren();

            EffectInstance = EffectInstance;
            EffectInstance?.ConnectChildren();

            _itemInstances?.ToList().ForEach(x => { x.MapCell = this; x.ConnectChildren(); });
        }
    }
}
