﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class Map : IHierarchical
    {
        public AtlasCell AtlasCell { get; set; }
        private Dictionary<int, MapColumn> _mapColumns = new Dictionary<int, MapColumn>();
        public AtlasColumn AtlasColumn
        {
            get
            {
                return AtlasCell == null ? null : AtlasCell.AtlasColumn;

            }
        }
        public Atlas Atlas
        {
            get
            {
                return AtlasColumn == null ? null : AtlasColumn.Atlas;
            }
        }
        public World World
        {
            get
            {
                return Atlas == null ? null : Atlas.World;
            }
        }
        public GameData Game
        {
            get
            {
                return World == null ? null : World.Game;
            }

        }
        public MapColumn this[int index]
        {
            get
            {
                return _mapColumns.GetSanitized(index);
            }
            set
            {
                _mapColumns.SetSanitized(index, value);
                _mapColumns[index].Map = this;
            }
        }

        public void SetMapColumn(int mapColumn, MapColumn value)
        {
            this[mapColumn] = value;
        }

        public MapColumn GetMapColumn(int mapColumn, bool autoCreate = false)
        {
            var result = this[mapColumn];
            if (result == null && autoCreate)
            {
                result = new MapColumn();
                SetMapColumn(mapColumn, result);
            }
            return result;
        }

        public void SetMapCell(int mapColumn, int mapRow, MapCell value)
        {
            if (GetMapColumn(mapColumn) == null)
            {
                SetMapColumn(mapColumn, new MapColumn());
            }
            this[mapColumn][mapRow] = value;
        }

        public MapCell GetMapCell(int mapColumn, int mapRow, bool autoCreate = false)
        {
            var result = GetMapColumn(mapColumn) == null ? null : this[mapColumn][mapRow];
            if (result == null && autoCreate)
            {
                result = new MapCell();
                SetMapCell(mapColumn, mapRow, result);
            }
            return result;
        }

        internal void Notify(string notification)
        {
            AtlasCell?.Notify(notification);
        }

        public void ConnectChildren()
        {
            if (_mapColumns != null)
            {
                _mapColumns.Values.ToList().ForEach(x => { x.Map = this; x.ConnectChildren(); });
            }
        }
    }
}
