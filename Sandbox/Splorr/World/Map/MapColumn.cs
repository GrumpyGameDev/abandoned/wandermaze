﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class MapColumn : IHierarchical
    {
        public Map Map { get; set; }
        private Dictionary<int, MapCell> _mapCells = new Dictionary<int, MapCell>();
        public AtlasCell AtlasCell
        {
            get
            {
                return Map == null ? null : Map.AtlasCell;
            }
        }
        public AtlasColumn AtlasColumn
        {
            get
            {
                return AtlasCell == null ? null : AtlasCell.AtlasColumn;

            }
        }
        public Atlas Atlas
        {
            get
            {
                return AtlasColumn == null ? null : AtlasColumn.Atlas;
            }
        }
        public World World
        {
            get
            {
                return Atlas == null ? null : Atlas.World;
            }
        }
        public GameData Game
        {
            get
            {
                return World == null ? null : World.Game;
            }
        }
        public MapCell this[int index]
        {
            get
            {
                return _mapCells.GetSanitized(index);
            }
            set
            {
                _mapCells = _mapCells.SetSanitized(index, value);
                _mapCells[index].MapColumn = this;
            }
        }
        public void SetMapCell(int mapRow, MapCell value)
        {
            this[mapRow] = value;
        }

        public MapCell GetMapCell(int mapRow, bool autoCreate = false)
        {
            var result = this[mapRow];
            if (result == null && autoCreate)
            {
                result = new MapCell();
                SetMapCell(mapRow, result);
            }
            return result;
        }
        public void ConnectChildren()
        {
            if (_mapCells != null)
            {
                _mapCells.Values.ToList().ForEach(x => { x.MapColumn = this; x.ConnectChildren(); });
            }
        }

        internal void Notify(string notification)
        {
            Map?.Notify(notification);
        }
    }
}
