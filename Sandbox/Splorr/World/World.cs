﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    
    public class World: IHierarchical
    {
        public GameData Game { get; set; }

        private Dictionary<int, Atlas> _atlases = new Dictionary<int, Atlas>();

        public MapCell this[Location location]
        {
            get
            {
                return GetMapCell(location.Atlas, location.AtlasColumn, location.AtlasRow, location.Map, location.MapColumn, location.MapRow);
            }
        }

        public void SetAtlas(int atlas, Atlas value)
        {
            this[atlas] = value;
        }

        public Atlas GetAtlas(int atlas, bool autoCreate = false)
        {
            var result = this[atlas];
            if (result == null && autoCreate)
            {
                result = new Atlas();
                SetAtlas(atlas, result);
            }
            return result;
        }

        public void SetAtlasColumn(int atlas, int atlasColumn, AtlasColumn value)
        {
            if(GetAtlas(atlas) == null)
            {
                SetAtlas(atlas, new Atlas());
            }
            this[atlas][atlasColumn] = value;
        }

        public AtlasColumn GetAtlasColumn(int atlas, int atlasColumn, bool autoCreate = false)
        {
            var result = this[atlas] == null ? null : this[atlas][atlasColumn];
            if (result == null && autoCreate)
            {
                result = new AtlasColumn();
                SetAtlasColumn(atlas, atlasColumn, result);
            }
            return result;
        }

        public void SetAtlasCell(int atlas, int atlasColumn, int atlasRow, AtlasCell value)
        {
            if(GetAtlasColumn(atlas,atlasColumn)== null)
            {
                SetAtlasColumn(atlas, atlasColumn, new AtlasColumn());
            }
            this[atlas][atlasColumn][atlasRow] = value;
        }

        public AtlasCell GetAtlasCell(int atlas, int atlasColumn, int atlasRow, bool autoCreate = false)
        {
            var result= GetAtlasColumn(atlas, atlasColumn) == null ? null : this[atlas][atlasColumn][atlasRow];
            if (result == null && autoCreate)
            {
                result = new AtlasCell();
                SetAtlasCell(atlas, atlasColumn, atlasRow, result);
            }
            return result;
        }

        public void SetMap(int atlas, int atlasColumn,int atlasRow, int map, Map value)
        {
            if(GetAtlasCell(atlas,atlasColumn,atlasRow)== null)
            {
                SetAtlasCell(atlas, atlasColumn, atlasRow, new AtlasCell());
            }
            this[atlas][atlasColumn][atlasRow][map] = value;
        }

        public Map GetMap(int atlas, int atlasColumn, int atlasRow, int map, bool autoCreate = false)
        {
            var result = GetAtlasCell(atlas, atlasColumn, atlasRow) == null ? null : this[atlas][atlasColumn][atlasRow][map];
            if (result == null && autoCreate)
            {
                result = new Map();
                SetMap(atlas, atlasColumn, atlasRow,map, result);
            }
            return result;
        }

        internal void Notify(string notification)
        {
            Game?.Notify(notification);
        }

        public void SetMapColumn(int atlas, int atlasColumn, int atlasRow, int map, int mapColumn, MapColumn value)
        {
            if(GetMap(atlas,atlasColumn,atlasRow, map)== null)
            {
                SetMap(atlas, atlasColumn, atlasRow, map, new Map());
            }
            this[atlas][atlasColumn][atlasRow][map][mapColumn] = value;
        }

        public MapColumn GetMapColumn(int atlas,int atlasColumn,int atlasRow,int map, int mapColumn, bool autoCreate = false)
        {
            var result = GetMap(atlas, atlasColumn, atlasRow, map) == null ? null : this[atlas][atlasColumn][atlasRow][map][mapColumn];
            if (result == null && autoCreate)
            {
                result = new MapColumn();
                SetMapColumn(atlas, atlasColumn, atlasRow, map, mapColumn, result);
            }
            return result;
        }

        public void SetMapCell(int atlas,int atlasColumn,int atlasRow,int map,int mapColumn,int mapRow, MapCell value)
        {
            if (GetMapColumn(atlas, atlasColumn, atlasRow, map, mapColumn) == null)
            {
                SetMapColumn(atlas, atlasColumn, atlasRow, map, mapColumn, new MapColumn());
            }
            this[atlas][atlasColumn][atlasRow][map][mapColumn][mapRow] = value;
        }

        public MapCell GetMapCell(int atlas, int atlasColumn, int atlasRow, int map, int mapColumn, int mapRow, bool autoCreate = false)
        {
            var result = GetMapColumn(atlas, atlasColumn, atlasRow, map,mapColumn) == null ? null : this[atlas][atlasColumn][atlasRow][map][mapColumn][mapRow];
            if (result == null && autoCreate)
            {
                result = new MapCell();
                SetMapCell(atlas, atlasColumn, atlasRow, map, mapColumn, mapRow, result);
            }
            return result;
        }

        public Atlas this[int index]
        {
            get
            {
                return _atlases.GetSanitized(index);
            }
            set
            {
                _atlases = _atlases.SetSanitized(index, value);
                _atlases[index].World = this;
            }
        }

        public void ConnectChildren()
        {
            if(_atlases!=null)
            {
                _atlases.Values.ToList().ForEach(x => { x.World = this; x.ConnectChildren(); });
            }
        }
    }
}
