﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public class GameData: IHierarchical, IScriptRunner
    {
        private Type _scriptTableType;
        public Type ScriptTableType
        {
            get
            {
                return _scriptTableType;
            }
            set
            {
                _scriptTableType = value;
                _scriptTable = null;
            }
        }
        [NonSerialized]
        private IScriptTable _scriptTable = null;
        public IScriptTable ScriptTable
        {
            get
            {
                if(_scriptTable== null)
                {
                    _scriptTable = Activator.CreateInstance(ScriptTableType) as IScriptTable;
                }
                return _scriptTable;
            }
        }

        private Player _player;
        public Player Player
        {
            get
            {
                return _player;
            }
            set
            {
                if (_player != null)
                {
                    _player.GameData = null;
                }
                _player = value;
                if (_player != null)
                {
                    _player.GameData = this;
                }
            }
        }

        private World _world;
        public World World
        {
            get
            {
                return _world;
            }
            set
            {
                if (_world != null)
                {
                    _world.Game = null;
                }
                _world = value;
                if (_world != null)
                {
                    _world.Game = this;
                }
            }
        }
        private Configuration _configuration;
        public Configuration Configuration
        {
            get
            {
                return _configuration;
            }
            set
            {
                if (_configuration != null)
                {
                    _configuration.Game = null;
                }
                _configuration = value;
                if (_configuration != null)
                {
                    _configuration.Game = this;
                }
            }
        }
        public GameData()
        {
        }

        public void ConnectChildren()
        {
            if(Player!= null)
            {
                Player.GameData = this;
                Player.ConnectChildren();
            }
            if(World!= null)
            {
                World.Game = this;
                World.ConnectChildren();
            }
            if(Configuration!= null)
            {
                Configuration.Game = this;
                Configuration.ConnectChildren();
            }
        }
        public static void Save(GameData game,string fileName)
        {
            IFormatter formatter = new BinaryFormatter();
            using (Stream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write, FileShare.None))
            {
                formatter.Serialize(stream, game);
                stream.Close();
            }
        }

        public static GameData Load(string fileName)
        {
            IFormatter formatter = new BinaryFormatter();
            GameData game = null;
            try
            {
                using (Stream stream = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    game = formatter.Deserialize(stream) as GameData;
                    game.ConnectChildren();
                    stream.Close();
                }
            }
            catch
            {
                //just eat it!
            }
            return game;
        }

        static GameData()
        {
        }

        private Queue<string> _notifications = new Queue<string>();

        public void Notify(string notification)
        {
            _notifications.Enqueue(notification);
        }

        public string CurrentNotification
        {
            get
            {
                return _notifications.Any() ? _notifications.Peek() : null;
            }
        }

        public void ClearNotification()
        {
            _notifications.Dequeue();
        }

        public void ClearAllNotifications()
        {
            _notifications.Clear();
        }

        public bool HasScript(string scriptName)
        {
            if(ScriptTable!= null)
            {
                return ScriptTable[scriptName] != null;
            }
            else
            {
                return false;
            }
        }

        public object RunScript(string scriptName, InstanceBase self, Descriptor selfDescriptor, InstanceBase other, Descriptor otherDescriptor, MapCell where)
        {
            if(ScriptTable!= null)
            {
                return ScriptTable[scriptName](self, selfDescriptor, other, otherDescriptor, where);
            }
            return null;
        }
    }
}
