﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    public interface IDescriptor: IHierarchical, IScriptRunner
    {
        Configuration Configuration { get; set; }
        string GetScript(string scriptName);
    }
}
