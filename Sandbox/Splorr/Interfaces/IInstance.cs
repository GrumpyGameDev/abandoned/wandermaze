﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    public interface IMapCellInstance: IHierarchical
    {
        MapCell MapCell { get; set; }
    }
}
