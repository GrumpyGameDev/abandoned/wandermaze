﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    public interface IScriptTable
    {
        Func<InstanceBase, Descriptor, InstanceBase, Descriptor, MapCell, object> this[string index] { get; set; }
    }
}
