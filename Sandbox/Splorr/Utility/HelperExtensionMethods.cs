﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    public static class HelperExtensionMethods
    {
        public static V GetSanitized<K,V>(this Dictionary<K,V> dictionary, K key)
        {
            if (dictionary == null)
            {
                return default(V);
            }
            else if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }
            else
            {
                return default(V);
            }
        }
        public static Dictionary<K,V> SetSanitized<K,V>(this Dictionary<K,V> dictionary, K key, V value)
        {
            if (dictionary == null)
            {
                dictionary = new Dictionary<K,V>();
            }
            dictionary[key] = value;
            return dictionary;
        }

        public static void SetImageName(this InstanceBase instance, string imageName)
        {
            instance.SetStringProperty(Properties.ImageName, imageName);
        }

        public static string GetImageName(this InstanceBase instance)
        {
            return instance.GetStringProperty(Properties.ImageName);
        }

        public static object RunOptionalScript(this IScriptRunner scriptRunner,string scriptName, InstanceBase self, Descriptor selfDescriptor, InstanceBase other, Descriptor otherDescriptor, MapCell where, object defaultResult)
        {
            if(scriptRunner.HasScript(scriptName))
            {
                return scriptRunner.RunScript(scriptName, self, selfDescriptor, other, otherDescriptor, where);
            }
            else
            {
                return defaultResult;
            }
        }
    }
}
