﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public abstract class InstanceBase: ScriptableBase
    {
        private Dictionary<string, string> _stringProperties;
        private Dictionary<string, int> _intProperties;
        private Dictionary<string, double> _doubleProperties;
        private Dictionary<string, bool> _boolProperties;

        public void SetStringProperty(string index, string value)
        {
            if (_stringProperties == null)
            {
                _stringProperties = new Dictionary<string, string>();
            }
            if (value == null)
            {
                if (_stringProperties.ContainsKey(index))
                {
                    _stringProperties.Remove(index);
                }
            }
            else
            {
                _stringProperties[index] = value;
            }
        }

        public void SetIntProperty(string index, int? value)
        {
            if (_intProperties == null)
            {
                _intProperties = new Dictionary<string, int>();
            }
            if (value == null)
            {
                if (_intProperties.ContainsKey(index))
                {
                    _intProperties.Remove(index);
                }
            }
            else
            {
                _intProperties[index] = value.Value;
            }
        }

        public void SetDoubleProperty(string index, double? value)
        {
            if (_doubleProperties == null)
            {
                _doubleProperties = new Dictionary<string, double>();
            }
            if (value == null)
            {
                if (_doubleProperties.ContainsKey(index))
                {
                    _doubleProperties.Remove(index);
                }
            }
            else
            {
                _doubleProperties[index] = value.Value;
            }
        }

        public void SetBoolProperty(string index, bool? value)
        {
            if (_boolProperties == null)
            {
                _boolProperties = new Dictionary<string, bool>();
            }
            if (value == null)
            {
                if (_boolProperties.ContainsKey(index))
                {
                    _boolProperties.Remove(index);
                }
            }
            else
            {
                _boolProperties[index] = value.Value;
            }
        }

        public string GetStringProperty(string index)
        {
            return _stringProperties.GetSanitized(index);
        }

        public int? GetIntProperty(string index)
        {
            return _intProperties.GetSanitized(index); 
        }

        public double? GetDoubleProperty(string index)
        {
            return _doubleProperties.GetSanitized(index);
        }

        public bool? GetBoolProperty(string index)
        {
            return _boolProperties.GetSanitized(index);
        }

    }
}
