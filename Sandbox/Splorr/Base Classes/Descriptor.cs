﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public abstract class Descriptor : ScriptableBase, IDescriptor
    {
        public Configuration Configuration { get; set; }

        public void SetNewScript(string script)
        {
            SetScript(Scripts.New, script);
        }

        public void SetOnSpawnScript(string script)
        {
            SetScript(Scripts.OnSpawn, script);
        }

        public override object RunScript(string scriptName, InstanceBase self, Descriptor selfDescriptor, InstanceBase other, Descriptor otherDescriptor, MapCell where)
        {
            return Configuration?.Game?.RunScript(GetScript(scriptName), self, selfDescriptor, other, otherDescriptor, where);
        }

        public abstract void ConnectChildren();
    }
}
