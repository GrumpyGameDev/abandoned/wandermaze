﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    [Serializable]
    public abstract class ScriptableBase: IScriptRunner
    {
        private Dictionary<string, string> _scripts;
        public string GetScript(string scriptName)
        {
            return _scripts.GetSanitized(scriptName);
        }

        public bool HasScript(string scriptName)
        {
            return _scripts.ContainsKey(scriptName);
        }
        public abstract object RunScript(string scriptName, InstanceBase self, Descriptor selfDescriptor, InstanceBase other, Descriptor otherDescriptor, MapCell where);

        public void SetScript(string scriptName, string script)
        {
            _scripts = _scripts.SetSanitized(scriptName, script);
        }
    }
}
