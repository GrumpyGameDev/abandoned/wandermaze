﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Splorr
{
    public static class Scripts
    {
        public const string New = "new";
        public const string OnSpawn = "onSpawn";
        public const string CanEnter = "canEnter";
        public const string OnEnter = "onEnter";
        public const string CanPickup = "canPickup";
        public const string OnPickup = "onPickup";
    }
}
